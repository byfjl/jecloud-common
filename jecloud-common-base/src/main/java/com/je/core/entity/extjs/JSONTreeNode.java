/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.core.entity.extjs;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author YUNFENGCHENG
 * @version 1.0
 * @用途 Node辅助类
 */
public class JSONTreeNode implements Serializable {

    private static final long serialVersionUID = -5445464065128298330L;

    /**
     * ID
     */
    private String id;
    /**
     * 节点显示
     */
    private String text;
    /**
     * 所在字典的名称
     */
    private String code;
    /**
     * 父节点ID
     */
    private String parent;
    /**
     * 图标
     */
    private String icon;
    /**
     * 图标颜色
     */
    private String iconColor;
    private String nodeType;
    private String layer;
    /**
     * 描述信息
     */
    private String description = "";
    /**
     * 是否叶子
     */
    private boolean leaf = false;
    /**
     * 是否可展开
     */
    private boolean expandable = true;
    /**
     * 默认是否展开
     */
    private boolean expanded = false;
    private boolean checked = false;
    /**
     * 是否异步
     */
    private boolean async = false;
    /**
     * 节点信息
     */
    private String nodeInfo = "";
    /**
     * 节点实体类型
     */
    private String nodeInfoType = "";
    /**
     * 路径
     */
    private String nodePath;
    /**
     * 是否禁用
     */
    private String disabled;
    /**
     * 排序
     */
    private String orderIndex = "";
    /**
     * 树形排序字段
     */
    private String treeOrderIndex = "";
    /**
     * 原业务实体需构建的业务字段
     */
    private Map<String, Object> bean = new HashMap<>();
    /**
     * 子节点
     */
    List<JSONTreeNode> children = new ArrayList<>();
    /**
     * 其他业务bean字段
     */
    List<String> otherBeanFiled = new ArrayList<>();

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isLeaf() {
        return leaf;
    }

    public void setLeaf(boolean leaf) {
        this.leaf = leaf;
    }

    public boolean isExpandable() {
        return expandable;
    }

    public void setExpandable(boolean expandable) {
        this.expandable = expandable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean getChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public List<JSONTreeNode> getChildren() {
        return children;
    }

    public void setChildren(List<JSONTreeNode> children) {
        this.children = children;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getNodeInfo() {
        return nodeInfo;
    }

    public void setNodeInfo(String nodeInfo) {
        this.nodeInfo = nodeInfo;
    }

    public String getNodeInfoType() {
        return nodeInfoType;
    }

    public void setNodeInfoType(String nodeInfoType) {
        this.nodeInfoType = nodeInfoType;
    }

    public String getDisabled() {
        return disabled;
    }

    public void setDisabled(String disabled) {
        this.disabled = disabled;
    }

    public String getNodePath() {
        return nodePath;
    }

    public void setNodePath(String nodePath) {
        this.nodePath = nodePath;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public Map<String, Object> getBean() {
        return bean;
    }

    public void setBean(Map<String, Object> bean) {
        this.bean = bean;
    }

    public String getOrderIndex() {
        return orderIndex;
    }

    public void setOrderIndex(String orderIndex) {
        this.orderIndex = orderIndex;
    }

    public String getLayer() {
        return layer;
    }

    public void setLayer(String layer) {
        this.layer = layer;
    }

    public String getTreeOrderIndex() {
        return treeOrderIndex;
    }

    public void setTreeOrderIndex(String treeOrderIndex) {
        this.treeOrderIndex = treeOrderIndex;
    }

    public boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public boolean isAsync() {
        return async;
    }

    public void setAsync(boolean async) {
        this.async = async;
    }

    public String getIconColor() {
        return iconColor;
    }

    public void setIconColor(String iconColor) {
        this.iconColor = iconColor;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public boolean isChecked() {
        return checked;
    }

    public List<String> getOtherBeanFiled() {
        return otherBeanFiled;
    }

    public void setOtherBeanFiled(List<String> otherBeanFiled) {
        this.otherBeanFiled = otherBeanFiled;
    }

    public void addOtherBeanFiled(String field) {
        this.otherBeanFiled.add(field);
    }


    @Override
    public String toString() {
        return "JSONTreeNode{" +
                "id='" + id + '\'' +
                ", text='" + text + '\'' +
                ", code='" + code + '\'' +
                ", parent='" + parent + '\'' +
                ", icon='" + icon + '\'' +
                ", iconColor='" + iconColor + '\'' +
                ", nodeType='" + nodeType + '\'' +
                ", layer='" + layer + '\'' +
                ", description='" + description + '\'' +
                ", leaf=" + leaf +
                ", expandable=" + expandable +
                ", expanded=" + expanded +
                ", checked=" + checked +
                ", async=" + async +
                ", nodeInfo='" + nodeInfo + '\'' +
                ", nodeInfoType='" + nodeInfoType + '\'' +
                ", nodePath='" + nodePath + '\'' +
                ", disabled='" + disabled + '\'' +
                ", orderIndex='" + orderIndex + '\'' +
                ", treeOrderIndex='" + treeOrderIndex + '\'' +
                ", bean=" + bean +
                ", children=" + children +
                ", otherBeanFiled=" + otherBeanFiled +
                '}';
    }
}

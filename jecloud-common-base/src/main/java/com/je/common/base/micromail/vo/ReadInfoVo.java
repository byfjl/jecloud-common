/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.micromail.vo;


import com.je.common.base.DynaBean;

/**
 * @Description //微邮阅读状态entity
 * @Auther: yuchunhui
 * @Date: 14 16
 * @Description:
 */

public class ReadInfoVo {
  /**
   * @Description //阅读人id
   **/
  private String userId;
  /**
   * @Description //阅读人
   **/
  private String userName;
  /**
   * @Description //阅读时间
   **/
  private String updateTime;
  /**
   * @Description //提醒次数
   **/
  private int txNumber;

  public static ReadInfoVo build(DynaBean dynaBean, String type) {
    //TODO
    ReadInfoVo readInfoVo = new ReadInfoVo();
    readInfoVo.setUserId(dynaBean.getStr("SY_CREATEUSERID"));
    readInfoVo.setUserName(dynaBean.getStr("SY_CREATEUSERNAME"));
    if(type.equals("1")){//阅读
      readInfoVo.setUpdateTime(dynaBean.getStr("INFO_YDSJ"));
    }else if (type.equals("2")){//点赞
      readInfoVo.setUpdateTime(dynaBean.getStr("INFO_DZSJ"));
    }
    readInfoVo.setTxNumber(3);
    return readInfoVo;
  }

  public static ReadInfoVo buildUser(DynaBean dynaBean) {
    //TODO
    ReadInfoVo readInfoVo = new ReadInfoVo();
    readInfoVo.setUserId(dynaBean.getStr("USERID"));
    readInfoVo.setUserName(dynaBean.getStr("USERNAME"));
    readInfoVo.setTxNumber(dynaBean.getInt("NUMBER"));
    return readInfoVo;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(String updateTime) {
    this.updateTime = updateTime;
  }

  public int getTxNumber() {
    return txNumber;
  }

  public void setTxNumber(int txNumber) {
    this.txNumber = txNumber;
  }
}

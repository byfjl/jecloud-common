/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table;

import com.je.common.base.constants.ConstantVars;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.table.service.BuildingSqlService;
import com.je.common.base.table.service.impl.*;

/**
 * SQL构建器工厂
 */
public class BuildingSqlFactory {

    /**
     * 构建SQL构建器
     * @return
     */
    public static BuildingSqlService build(){

        //mysql
        if(ConstantVars.STR_MYSQL.equals(JEDatabase.getCurrentDatabase())){
            return SpringContextHolder.getBean(BuildingMySqlServiceImpl.class);
        }
        if(ConstantVars.STR_ORACLE.equals(JEDatabase.getCurrentDatabase())){
            return SpringContextHolder.getBean(BuildingOracleServiceImpl.class);
        }
        //kingbase
        if(ConstantVars.STR_KINGBASEES.equals(JEDatabase.getCurrentDatabase())){
            return SpringContextHolder.getBean(BuildingKingbaseServiceImpl.class);
        }
        //dameng
        if(ConstantVars.STR_DM.equals(JEDatabase.getCurrentDatabase())){
            return SpringContextHolder.getBean(BuildingDmServiceImpl.class);
        }
        //tidb
        if(ConstantVars.STR_TIDB.equals(JEDatabase.getCurrentDatabase())){
            return SpringContextHolder.getBean(BuildingTiDbServiceImpl.class);
        }
        //sqlserver
        if(ConstantVars.STR_SQLSERVER.equals(JEDatabase.getCurrentDatabase())){
            return SpringContextHolder.getBean(BuildingSqlServerServiceImpl.class);
        }
        //oscar
        if(ConstantVars.STR_SHENTONG.equals(JEDatabase.getCurrentDatabase())){
            return SpringContextHolder.getBean(BuildingOscarServiceImpl.class);
        }

        throw new RuntimeException("不支持的数据库版本...");

    }

}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.dd;

/**
 * 数据字典类型
 */
public class DDType {

	/**
	 * 列表字典
	 */
	public static final String LIST = "LIST";
	
	/**
	 * 树形字典
	 */
	public static final String TREE = "TREE";

	/**
	 * 外部树形字典
	 */
	public static final String DYNA_TREE = "DYNA_TREE";

	/**
	 * 实体树形字典
	 */
	public static final String MODEL_TREE = "MODEL_TREE";

	/**
	 * 自定义
	 */
	public static final String CUSTOM="CUSTOM";

	/**
	 * SQL动态字典
	 */
	public static final String SQL = "SQL";

	/**
	 * SQL动态树形字典
	 */
	public static final String SQL_TREE = "SQL_TREE";
}

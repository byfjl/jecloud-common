/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service;

import com.je.common.base.DynaBean;
import com.je.common.base.exception.TreeFuncException;
import com.je.core.entity.extjs.JSONTreeNode;

import java.util.List;

public interface CommonTreeService {

    /**
     * 保存单根树
     *
     * @param funcCode
     * @param dynaBean
     * @param codeGenFieldInfo
     * @param batchFilesFields
     * @param uploadableFields
     * @return
     * @throws TreeFuncException
     */
    DynaBean doSingleSave(String funcCode, DynaBean dynaBean, String codeGenFieldInfo, String batchFilesFields, String uploadableFields);

    /**
     * 保存单根树
     *
     * @param funcCode
     * @param dynaBean
     * @param codeGenFieldInfo
     * @param batchFilesFields
     * @param uploadableFields
     * @return
     * @throws TreeFuncException
     */
    DynaBean doMoreRootSave(String funcCode, DynaBean dynaBean, String codeGenFieldInfo, String batchFilesFields, String uploadableFields);

    /**
     * 只构建此树形节点
     *
     * @param dynaBean
     * @return
     */
    JSONTreeNode buildSingleTreeNode(DynaBean dynaBean);

    /**
     * 单根树更新集合
     *
     * @param tableCode
     * @param updateStr
     * @param funcType
     * @param funcCode
     * @param codeGenFieldInfo
     * @return
     */
    List<DynaBean> doSingleUpdateList(String tableCode, String updateStr, String funcType, String funcCode, String codeGenFieldInfo);

    /**
     * 多根树更新集合
     *
     * @param tableCode
     * @param updateStr
     * @param funcType
     * @param funcCode
     * @param codeGenFieldInfo
     * @return
     */
    List<DynaBean> doMoreRootUpdateList(String tableCode, String updateStr, String funcType, String funcCode, String codeGenFieldInfo);

    /**
     *
     * @param tableCode
     * @param updateStr
     * @param funcType
     * @param funcCode
     * @param codeGenFieldInfo
     * @param subFuncForeignKey 子功能集合外键编码
     * @return
     */
    List<DynaBean> doMoreRootUpdateList(String tableCode, String updateStr, String funcType, String funcCode, String codeGenFieldInfo,String subFuncForeignKey);

    /**
     * 查询树形功能list
     *
     * @param tableCode
     * @param searchName
     * @param ddCode
     * @return
     */
    List<DynaBean> getTreeListBySearchName(String tableCode, String searchName, String ddCode);
}

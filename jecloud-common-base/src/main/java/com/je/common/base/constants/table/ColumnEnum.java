/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.table;

public enum ColumnEnum {

    ID ("主键"),
    VARCHAR30 ("字符串30"),
    VARCHAR50 ("字符串50"),
    VARCHAR100( "字符串100"),
    VARCHAR255 ("字符串255"),
    VARCHAR767 ("字符串767"),
    VARCHAR1000 ("字符串1000"),
    VARCHAR2000 ("字符串2000"),
    VARCHAR4000 ("字符串4000"),
    VARCHAR ("字符串"),
    NUMBER ( "整数"),
    DATE ("日期"),
    DATETIME ("日期时间"),
    FLOAT("小数"),
    FLOAT2("小数(2)"),
    YESORNO ("是非"),
    CLOB("超大文本"),
    BIGCLOB("巨大文本"),
    FOREIGNKEY("外键"),
    CUSTOM("自定义"),
    CUSTOMFOREIGNKEY("外键自定义"),
    CUSTOMID("主键自定义");

    String desc;

    ColumnEnum( String desc){
        this.desc = desc;
    }

    public String getDesc() {
        return desc;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.api;

public class InterfaceConstant {
    //数据来源
    public static final String DATA_SOURCE_FUNC="func";
    public static final String DATA_SOURCE_TABLE="table";
    //服务类型
    public static final String SERVICE_TYPE_QUERY="query";
    public static final String SERVICE_TYPE_INSERT="insert";
    public static final String SERVICE_TYPE_UPDATE="update";
    public static final String SERVICE_TYPE_DELETE="delete";

    //构建方式
    public static final String DATA_BUILDTYPE_NOCODE="noCode";
    public static final String DATA_BUILDTYPE_CUSTOM="custom";
    //启用、未启用
    public static final String DATA_STATUS_ENABLE="1";
    public static final String DATA_STATUS_DISABLED="0";

    //是否自定义验证
    public static final String CUSTOM_ENABLE="1";
    public static final String CUSTOM_DISABLE="0";

    //字符验证
    public static final String CHARACTER_VALIDATION_DISABLED="close";
    public static final String CHARACTER_VALIDATION_ALL="all";
    public static final String CHARACTER_VALIDATION_CUSTOM="custom";

    public static final String CHARACTER_VALIDATION_PATTERN_PREFIX ="[";
    public static final String CHARACTER_VALIDATION_PATTERN_SUFFIX ="]";

    //短语验证
    public static final String[] CRUDKEYWORD={"SELECT","DELETE","INSERT","UPDATE"};

    public static final String PHRASE_VALIDATION_DISABLED="close";
    public static final String PHRASE_VALIDATION_ALL="all";
    public static final String PHRASE_VALIDATION_CUSTOM="custom";

    //参数字段类型
    public static final String PARAMETER_TYPE_STRING="string";
    public static final String PARAMETER_TYPE_INT="int";
    public static final String PARAMETER_TYPE_BOOLEAN="boolean";
    public static final String PARAMETER_TYPE_DOUBLE="double";

    public static final int INT_MAX =2147483647;
    public static final int INT_MIN =-2147483648;

    //需要加密的key
    public static final String[] NEED_ENCRYPTION_KEY={"userPassword","parameter"};
    //不需要加密的key
    public static final String[] NO_NEED_ENCRYPTION_KEY={"apiCode","userCode"};
    //不加密
    public static final String NO_ENCRYPTION="-1";

    //是非
    public static final String COMMON_YES="1";
    public static final String COMMON_NO="0";
    //清理日志
    public static final String CLEAR_LOG_NEVER="never";
    public static final String CLEAR_LOG_MONTHLY="monthly";
    public static final String CLEAR_LOG_QUARTERLY="quarterly";
    public static final String CLEAR_LOG_HALFYEAR="halfYear";
    public static final String CLEAR_LOG_EVERYYEAR="everyYear";

    //接口地址
    public static final String URL="je/api/execute";

}

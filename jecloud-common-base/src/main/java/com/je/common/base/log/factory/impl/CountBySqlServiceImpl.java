/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.log.factory.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.log.factory.SelectService;
import com.je.common.base.util.StringUtil;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.HashMap;

public class CountBySqlServiceImpl implements SelectService {

    private static final HashMap<String,String> map = new HashMap();

    static {
        map.put("sql@params","countBySql2p");
        map.put("wrapper","countBySqlByWrapper1p");
        map.put("tableCode@sql","countBySqlByTableCode2p");
        map.put("tableCode@sql@params","countBySqlByTableCode3p");
    }

    @Override
    public String getSql(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodType = getMethodType(signature.getMethod(),map);

        JSONObject jsonObject = new JSONObject();
        switch (methodType){
            case "countBySql2p":
                jsonObject =countBySql2p(joinPoint.getArgs());
                break;
            case "countBySqlByWrapper1p":
                jsonObject =countBySqlByWrapper1p(joinPoint.getArgs());
                break;
            case "countBySqlByTableCode2p":
                jsonObject =countBySqlByTableCode2p(joinPoint.getArgs());
                break;
            case "countBySqlByTableCode3p":
                jsonObject =countBySqlByTableCode3p(joinPoint.getArgs());
                break;

        }
        return JSON.toJSONString(jsonObject);
    }

    private JSONObject countBySqlByTableCode2p(Object[] args) {
        if(args==null || args.length!=2){
            return null;
        }
        String tableCode = (String) args[0];
        String sql = (String) args[1];
        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
        if(StringUtil.isNotEmpty(tableCode)){
            conditionsWrapper.table(tableCode);
        }
        if(StringUtil.isNotEmpty(sql)){
            conditionsWrapper.apply(sql,null);
        }
        return   getSqlInfoByWrapper(conditionsWrapper);

    }

    private JSONObject countBySqlByWrapper1p(Object[] args) {
        if(args==null || args.length!=1){
            return null;
        }

        if(args[0] instanceof ConditionsWrapper ){
            ConditionsWrapper conditionsWrapper = (ConditionsWrapper) args[0];
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(sql_text,conditionsWrapper.getParameterSql());
            jsonObject.put(sql_parameter,"");
            return jsonObject;
        }

        return null;
    }

    private JSONObject countBySql2p(Object[] args) {
        if(args==null || args.length!=2){
            return null;
        }

        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
        String sql = (String) args[0];
        Object[] params = (Object[]) args[1];
        conditionsWrapper.apply(sql,params);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put(sql_text,conditionsWrapper.getParameterSql());
        jsonObject.put(sql_parameter,"");
        return jsonObject;

    }

    private JSONObject countBySqlByTableCode3p(Object[] args) {
        if(args==null || args.length!=3){
            return null;
        }
        String tableCode = (String) args[0];
        String sql = (String) args[1];
        Object[] params = (Object[]) args[2];
        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
        conditionsWrapper.table(tableCode).apply(sql, params);
        return getSqlInfoByWrapper(conditionsWrapper);
    }
}

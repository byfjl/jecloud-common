/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service;

import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;

public enum StatisticsEnum {
    /**
     * 和
     */
    SUM("sum"),
    /**
     * 条数
     */
    COUNT("count"),
    /**
     * 最大值
     */
    MAX("max"),
    /**
     * 最小值
     */
    MIN("min"),
    /**
     * 平均值
     */
    AVERAGE("average");

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    StatisticsEnum(String value) {
        this.value = value;
    }


    /**
     * 如果有数据库差异，可以在这里区分
     *
     * @param type
     * @return
     */
    public static String getSqlTypeByStatisticsType(String type) {
        if (SUM.getValue().equals(type)) {
            return SUM.toString();
        } else if (COUNT.getValue().equals(type)) {
            return COUNT.toString();
        } else if (MAX.getValue().equals(type)) {
            return MAX.toString();
        } else if (MIN.getValue().equals(type)) {
            return MIN.toString();
        } else if (AVERAGE.getValue().equals(type)) {
            return "AVG";
        } else if (type.equals("msg")) {//前端特殊处理，不需要运算，直接返给前端
            return "";
        }
        if (StatisticsEnum.valueOf(type) == null) {
            throw new PlatformException(type + "统计类型错误！", PlatformExceptionEnum.STATISTICS_TYPE_ERROR);
        }
        return "";
    }

}

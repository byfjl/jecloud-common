/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants;
/**
 * 登录错误信息
 * @author zhangshuaipeng
 *
 */
public class LoginErrorType {
	/**
	 * 没有该用户
	 */
	public static final String NONE = "NONE";
	/**
	 * 用户被禁用
	 */
	public static final String DISABLED="DISABLED";
	/**
	 * 该用户已经失效
	 */
	public static final String INVALID="INVALID";
	/**
	 * 无效的验证码
	 */
	public static final String ERRORCODE="ERRORCODE";
	/**
	 * 该用户不是系统用户
	 */
	public static final String NOSYS="NOSYS";
	/**
	 * 该用户在别处登录
	 */
	public static final String OTHERLOGIN="OTHERLOGIN";
	/**
	 * 用户密码错误
	 */
	public static final String ERRORPASSWORD="ERRORPASSWORD";
	/**
	 * 用户密码错误
	 */
	public static final String ERRORPROXY="ERRORPROXY";
	/**
	 * 提醒锁定
	 */
	public static final String WARNLOCKED="WARNLOCKED";
	/**
	 * 需要输入验证码错误标识
	 */
	public static final String WARNCODE="WARNCODE";
	/**
	 * 用户锁定
	 */
	public static final String USERLOCKED="USERLOCKED";
	/**
	 * 该用户没有部门
	 */
	public static final String ERRORDEPT="ERRORDEPT";
	/**
	 * 未注册
	 */
	public static final String NOREGISTER="NOREGISTER";
	/**
	 * 验证码错误
	 */
	public static final String ERRORJCAPTCHA="ERRORJCAPTCHA";
	/**
	 * 无效的操作密钥
	 */
	public static final String ERRORLOGINKEY="ERRORLOGINKEY";
	/**
	 * 该密钥已失效
	 */
	public static final String INVALIDLOGINKEY="INVALIDLOGINKEY";
	/**
	 * 用户登录次数限制异常
	 */
	public static final String USERNUMERROR="USERNUMERROR";
	/**
	 * 该密钥已失效
	 */
	public static final String OTHER="OTHER";
	/**
	 * 用户未认证
	 */
	public static final String NOTCERTIFIED="NOTCERTIFIED";
	/**
	 * 用户认证失败
	 */
	public static final String AUTHERROR="AUTHERROR";
	/**
	 * 注册重复邮箱
	 */
	public static final String REGISTERREPEATEMAIL="REGISTERREPEATEMAIL";
	/**
	 * 注册重复手机号
	 */
	public static final String REGISTERREPEATPHONE="REGISTERREPEATPHONE";
	/**
	 * 认证已存在
	 */
	public static final String REGISTERREPEATOPENID="REGISTERREPEATOPENID";

	/**
	 * 不允许此方式登录
	 */
	public static final String NOT_PERMD_LOGIN_TYPE="NOT_PERMD_LOGIN_TYPE";

	/**
	 * 系统配置错误
	 */
	public static final String SYS_CONFIG_ERROR="SYS_CONFIG_ERROR";
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.rbac;


public class PermType {
	/** 菜单权限 */
	public static final String MENU = "MENU";
	public static final String MT = "MT";
	public static final String URL = "URL";
	public static final String IDDT = "IDDT";
	public static final String PORTAL = "PORTAL";
	public static final String DIC = "DIC";
	
	public static final String IFRAME = "IFRAME";
	/** 功能权限 */
	public static final String FUNC = "FUNC";
	/** 子功能权限 */
	public static final String SUB_FUNC = "SUB_FUNC";
	/** 按钮权限 */
	public static final String BUTTON = "BUTTON";
	/** 菜单类型*/
	public static final String MENU_TYPE = "MT,MENU,URL,IDDT,IFRAME,PORTAL,DIC";
	/**手机App*/
	public static final String APP="app";
	/**手机菜单*/
	public static final String APPMENU="appmenu";
	/**手机功能*/
	public static final String APPFUNC="appfunc";
	/**手机按钮*/
	public static final String APPBUTTON="appbutton";
	/**手机子功能*/
	public static final String APPCHILDFUNC="appchildfunc";
}

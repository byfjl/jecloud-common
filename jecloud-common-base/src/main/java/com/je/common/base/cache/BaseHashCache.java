/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.cache;

import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-common
 * @author: LIULJ
 * @create: 2021-03-13 11:09
 * @description: Hash缓存服务定义
 */
public interface BaseHashCache<V> extends BaseContainerCache<V> {

    V getCacheValue(String subKey);

    V getCacheValue(String tenantId,String subKey);

    Map<String,V> getCacheValues();

    Map<String,V> getCacheValues(String tenantId);

    List<V> getCacheValues(List<String> subKeys);

    List<V> getCacheValues(String tenantId,List<String> subKeys);

    void putCache(String subKey, V value);

    void putCache(String tenantId,String subKey, V value);

    void putMapCache(Map<String,V> map);

    void putMapCache(String tenantId,Map<String,V> map);

    void removeCache(String subKey);

    void removeCache(String tenantId,String subKey);

    void removeCache(List<String> subKeys);

    void removeCache(String tenantId,List<String> subKeys);

    /**
     * 移除本地的缓存KEY
     * @param key
     */
    void localRemove(List<String> key);

    /**
     * 移除本地的租户的缓存KEY
     * @param tenantId
     * @param key
     */
    void localRemove(String tenantId,List<String> key);
}

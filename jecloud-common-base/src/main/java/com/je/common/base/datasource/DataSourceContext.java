/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.datasource;

import com.je.common.base.spring.SpringContextHolder;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.SQLException;

/**
* Copyright: Copyright (c) 2018 jeplus.cn
* @Description: 数据源上下文对象
* @version: v1.0.0
* @author: LIULJ
* @date: 2018年4月16日 下午5:24:43
*
* Modification History:
* Date         Author          Version            Description
*---------------------------------------------------------*
* 2018年4月16日     LIULJ           v1.0.0               初始创建
*
*
*/
public class DataSourceContext implements Serializable {

	private static final long serialVersionUID = -4689604766212153357L;
	private String dataSourceName;
	private DataSource dataSource;
	private Connection connection;

	public DataSourceContext(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}

	public DataSource getDataSource() {
		if(dataSource == null) {
			dataSource = SpringContextHolder.getBean(dataSourceName);
		}
		return dataSource;
	}

	public Connection getConnection() throws SQLException {
		getDataSource();
		if(dataSource == null) {
			throw new RuntimeException("---------------------根据名称" + dataSourceName + "获取数据源失败---------------------");
		}
		if(connection == null || connection.isClosed()) {
			connection = dataSource.getConnection();
		}
		return connection;
	}

	public void close() throws SQLException {
		if(connection != null) {
			connection.close();
		}
	}
}

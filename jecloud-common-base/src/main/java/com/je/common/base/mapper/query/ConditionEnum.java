/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.mapper.query;

/**
 * ConditionType
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/12/10
 */
public enum ConditionEnum {
    /**
     * 包含
     */
    IN("in"),
    /**
     * 包含
     */
    IN_SELECT("inSelect"),
    /**
     * 不包含
     */
    NOT_IN("notIn"),
    /**
     * 不包含
     */
    NOT_IN_SELECT("notInSelect"),
    /**
     * 不为空
     */
    NOT_NULL("notNull"),
    /**
     * 为空
     */
    IS_NULL("isNull"),

    /**
     * between
     */
    BETWEEN("between"),
    /**
     * 不等于
     */
    NE("!="),
    /**
     * 大于
     */
    GT(">"),
    /**
     * 大于等于
     */
    GE(">="),
    /**
     * 小于
     */
    LT("<"),
    /**
     * 小于等于
     */
    LE("<="),
    /**
     * 左模糊查询
     */
    LIKE_LEFT("%like"),
    /**
     * 右模糊查询
     */
    LIKE_RIGHT("like%"),
    /**
     * 模糊查询
     */
    LIKE("like"),
    /**
     * 等于
     */
    EQ("="),
    /**
     * and嵌套
     */
    AND("and"),
    /**
     * or嵌套
     */
    OR("or");

    /**
     * 条件类型
     */
    private String type;

    ConditionEnum(String type) {
        this.type = type;
    }

    /**
     * 获取类型枚举
     *
     * @param type 类型名称
     * @return com.je.core.mapper.query.ConditionEnum
     */
    public static ConditionEnum getCondition(String type) {
        for (ConditionEnum value : ConditionEnum.values()) {
            if (value.getType().equals(type)) {
                return value;
            }
        }
        return null;
    }

    public String getType() {
        return type;
    }
}

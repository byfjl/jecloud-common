/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import cn.hutool.core.io.IoUtil;
import com.google.common.base.Strings;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * propties文件的工具类  只能做一次修改
 * @author zhangshuaipeng
 *
 */
public class PropFileUtils {
	private static Logger logger= LoggerFactory.getLogger(PropFileUtils.class);
	private Properties properties=new Properties();
	private FileInputStream fis=null;
	private FileOutputStream fos=null;
	public PropFileUtils(File file){
		try{
		fis = new FileInputStream(file);
		fos = new FileOutputStream(file);
		properties.load(fis);
		}catch(Exception e){
			throw new PlatformException("配置文件工具类读取文件异常", PlatformExceptionEnum.JE_CORE_UTIL_PROP_READFILE_ERROR,new Object[]{file.getPath()},e);
		}
	}
	public PropFileUtils(File file,boolean write,boolean read){
		try{
		if(read){
			fis = new FileInputStream(file);
			properties.load(fis);
		}
		if(write){
			fos = new FileOutputStream(file);
		}
		}catch(Exception e){
			throw new PlatformException("配置文件工具类读取文件异常", PlatformExceptionEnum.JE_CORE_UTIL_PROP_READFILE_ERROR,new Object[]{file.getPath()},e);
		}
	}
	public String readKey(String key){
		if(properties.containsKey(key)){
			return properties.getProperty(key);
		}else{
			return "";
		}
	}

	public boolean containsKey(String key){
		return properties.contains(key);
	}

	public void put(String key,String value){
		if(Strings.isNullOrEmpty(value)){
			properties.setProperty(key, "");
			return;
		}
		properties.setProperty(key, value);
	}
	
	/**
	 * 添加键值对，添加完流关闭
	 * @param keys
	 * @param values
	 */
	public void createFile(List<String> keys,List<String> values){
		for(int i=0;i<keys.size();i++){
			properties.put(keys.get(i), values.get(i));			
		}
		try {
			properties.store(fos, "");
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			IoUtil.close(fis);
			IoUtil.close(fos);
		}
	}

	public void closeWrite(){
		try {
			properties.store(fos, "");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			IoUtil.close(fos);
		}
	}

	public void close(){
		IoUtil.close(fis);
		IoUtil.close(fos);
	}
}

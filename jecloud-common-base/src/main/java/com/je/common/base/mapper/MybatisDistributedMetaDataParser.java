/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.mapper;

import com.je.ibatis.extension.metadata.model.Function;
import com.je.ibatis.extension.metadata.model.Table;
import com.je.ibatis.extension.parse.MetaDataParse;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

/**
 * je-ibatis微服务元数据解析器，用于从元数据服务获取相关信息
 */
public class MybatisDistributedMetaDataParser implements MetaDataParse {

    @Override
    public List<Table> tables() {
        return null;
    }

    @Override
    public List<Table> tables(List<String> list) {
        return null;
    }

    @Override
    public List<Function> functions() {
        return null;
    }

    @Override
    public Table table(String s) {
        return null;
    }

    @Override
    public Function function(String s) {
        return null;
    }

    @Override
    public void setDataSource(DataSource dataSource) {

    }

    @Override
    public boolean formData(Table table, Map<String, Object> map) {
        return false;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.listener;


import com.je.common.base.constants.ConstantVars;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.spring.SpringContextHolder;
import org.apache.servicecomb.core.BootListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

public class DatabaseInitListener implements BootListener {
    public Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public int getOrder() {
        return 0;
    }

    @Override
    public void onAfterRegistry(BootEvent event) {
        Environment environment = SpringContextHolder.getBean(Environment.class);
        String dbName= environment.getProperty("jdbc.dialect");
        dbName=dbName.toLowerCase();
        if(dbName.indexOf("oracle")>=0){
            JEDatabase.setCurrentDatabase(ConstantVars.STR_ORACLE);
        }else if(dbName.indexOf("sqlserver")>=0){
            JEDatabase.setCurrentDatabase(ConstantVars.STR_SQLSERVER);
        }else if(dbName.indexOf("mysql")>=0){
            JEDatabase.setCurrentDatabase(ConstantVars.STR_MYSQL);
        } else if(dbName.indexOf("oscar")>=0){
            JEDatabase.setCurrentDatabase(ConstantVars.STR_SHENTONG);
        } else if(dbName.indexOf("kingbase8")>=0){
            JEDatabase.setCurrentDatabase(ConstantVars.STR_KINGBASEES);
        } else if(dbName.indexOf("dm")>=0){
            JEDatabase.setCurrentDatabase(ConstantVars.STR_DM);
        } else if (dbName.indexOf("tidb") > 0) {
            JEDatabase.setCurrentDatabase(ConstantVars.STR_TIDB);
        } else {
            JEDatabase.setCurrentDatabase(ConstantVars.STR_MYSQL);
        }
    }

}

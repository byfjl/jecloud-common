/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.micromail.service;

import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.micromail.vo.CommentVo;
import com.je.common.base.micromail.vo.MicromailVo;
import java.util.Map;

/**
 * @Description //微邮service
 * @Auther: yuchunhui
 * @Date 2019/10/10
 * @Param null:
 * @return: null
 **/
public interface MicromailService {

  /**
   * @Description //获取微邮数量
   * @Date 2019/10/10
   * @Param userId:用户id
   * @Param modelId:数据id
   * @return: int 数量
   **/
  Map<String,Object> getMicromailTotalTypeCount(String userId, String modelId);

  /**
   * @Description //获取微邮数量
   * @Date 2019/10/10
   * @Param userId:用户id
   * @Param modelId:数据id
   * @return: int 数量
   **/
  int getMicromailTotalCount(String userId, String modelId);


  /**
   * @Description //获取我收到的、我发出的、我关注的数量未读
   * @Date 2019/10/10
   * @Param userId:用户id
   * @Param modelId:数据id
   * @return: java.util.Map<java.lang.String, java.lang.Object>
   **/
  Map<String, Object> getNoReadCount(String userId, String modelId);

  /**
   * @Description //获取我收到的、我发出的、我关注的数量未读总数
   * @Date 2019/10/10
   * @Param userId:用户id
   * @Param modelId:数据id
   * @return: java.util.Map<java.lang.String, java.lang.Object>
   **/
  long getNoReadsCount(String userId);


  /**
   * @Description //创建微邮
   * @Date 2019/10/10
   * @Param recipient: 收件人
   * @Param bm:是否保密 1 true 0 false
   * @Param fbfs: 是否分别发送 1 true 0 false
   * @Param sy: 水印 1 true 0 false
   * @Param kftl:开放讨论
   * @Param zsr:主送人
   * @Param zsrId:主送人Id
   * @Param zt:主题
   * @Param zw:正文
   * @Param file:附件
   * @Param funcId:功能id
   * @Param funcCode:功能Code
   * @Param funcName:功能名称
   * @Param tableCode:表名
   * @Param modelId:数据id
   * @Param modelName:数据名称
   * @return: java.lang.String bm, fbfs, kftl, sy,
   **/
  Map<String, Object> createMicromail(MicromailVo micromailVo);

  /**
   * @Description //获取微邮
   * @Date 2019/10/10
   * @Param id: 微邮id
   * @return: com.je.core.util.bean.DynaBean
   **/
  MicromailVo getMicromailById(String id);


  /**
   * @Description //添加阅读记录
   * @Date 2019/10/10
   * @Param id:微邮id
   * @Param type:1 阅读 2 点赞 3 关注
   * @return: java.util.List<com.je.core.util.bean.DynaBean>
   **/
  void addReadMicromail(String id, String userId, String type);


  /**
   * @Description //发送未阅提醒
   * @Date 2019/10/10
   * @Param id:微邮id
   * @Param userId:用户id
   * @return: java.lang.String
   **/
  Map<String, Object> toMicromailRemind(String id, String userId);

  /**
   * @Description //删除微邮
   * @Date 2019/10/10
   * @Param id: 微邮id
   * @return: int
   **/
  Map<String, Object> deleteMicromail(String id);

  /**
   * @Description //删除评论
   * @Date 2019/10/10
   * @Param id: 微邮id
   * @return: int
   **/
  Map<String, Object> deleteComment(String id);

  /**
   * @Description //添加评论
   * @Date 2019/10/11
   * @Param id: 微邮id
   * @Param hfr: 回复人
   * @Param hfrId: 回复人id
   * @Param file: 附件
   * @Param zw: 正文
   * @Param fileId: 父级id
   * @return: void
   **/
  CommentVo createComment(String id, String hfr, String hfrId, String file, String zw, String fileId);

  /**
   * @Description //定时发送通知
   * @Date  2019/10/20
   * @return: void
  **/
  void sendMsg();

  //<<<<<<<<<<<<<<<<<<<<<<<<<<查询list

  /**
   * @Description //获取评论列表
   * @Date 2019/10/10
   * @Param id:微邮id
   * @Param pageNumber:最后一条记录id
   * @Param limit:数量
   * @return: java.util.List<com.je.core.util.bean.DynaBean>
   **/
  Map<String, Object> getMicromailComments(String id, String pageNumber, String limit);

  /**
   * @Description //获取未阅列表
   * @Date 2019/10/10
   * @Param id: 微邮id
   * @Param lasgMsgIdId:最后一个未阅列表id
   * @Param limit:数量
   * @return: java.util.List<com.je.core.util.bean.DynaBean>
   **/
  Map<String, Object> getNoReadMicromailList(String id, String lasgMsgIdId, String limit);

  /**
   * @Description //获取微邮已赞数据
   * @Date 2019/10/10
   * @Param id:微邮id
   * @Param pageNumber:查询赞数据最后一条记录id
   * @Param limit:数量
   * @return: java.util.List<com.je.core.util.bean.DynaBean>
   **/
  Map<String, Object> getMicromailUpsById(String id, String pageNumber, String limit);

  /**
   * @Description //获取已阅列表
   * @Date 2019/10/10
   * @Param id:微邮id
   * @Param lasgMsgIdId:最后一条已阅列表id
   * @Param limit:数量
   * @return: java.util.List<com.je.core.util.bean.DynaBean>
   **/
  Map<String, Object> getReadMicromailList(String id, String lasgMsgIdId, String limit);

  /**
   * @Description //获取我的微邮
   * @Date 2019/10/10
   * @Param userId:用户id
   * @Param modelId:数据id
   * @Param keyWord:关键字
   * @Param startTime:开始时间
   * @Param endTime:结束时间
   * @Param readType:阅读类型 1全部 2已读 3未读
   * @Param type:微邮类型 1我收到的 2我发出的 3我关注的
   * @Param pageNumber:最后一条记录id
   * @Param limit:获取数量
   * @return: java.util.List<com.je.core.util.bean.DynaBean>
   **/
  Map<String, Object> getMyMicromailLists(String userId, String modelId, String keyWord, String startTime, String endTime, String readType, String type, String pageNumber, String limit);

  Map<String,Integer> getMicromailCount(String modelIds);

  /**
   * 获取微邮的badge
   * @param userId
   * @return
   */
  JSONObject getRzNumWyNum(String userId);

  /**
   * 获取微邮的badge
   * @param userId
   * @return
   */
  JSONObject getWyBadge(String userId);

}

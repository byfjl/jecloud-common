/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants;
/**
 * 复制功能的依赖关系
 * @author zhangshuaipeng
 *
 */
public class FunCopyType {
	/**
	 * 普通
	 */
	public static final String PT = "PT";
	/**
	 * 只读硬连接
	 */
	public static final String ZDYLJ = "ZDYLJ";
	/**
	 * 正向软连接
	 */
	public static final String ZXRLJ = "ZXRLJ";
	/**
	 * 母体
	 */
	public static final String MT = "MT";
	/**
	 * 子体
	 */
	public static final String ZT = "ZT";
	public static final String FUN_SOFT_ICON="/core/css/imgs/tree/tree_func_soft.png";
	
}

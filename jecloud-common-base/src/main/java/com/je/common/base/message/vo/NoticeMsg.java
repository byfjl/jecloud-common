/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.message.vo;

/**
 * @ClassName
 * @Author wangchao
 * @Date 2022/6/22 0022 13:10
 * @Version V1.0
 */
public class NoticeMsg {
    /**
     * 接收人Id
     */
    private String targetUserId;
    /**
     * 接收人
     */
    private String targetUserName;
    /**
     * 接收人部门Id
     */
    private String targetUserDeptId;
    /**
     * 接收人部门
     */
    private String targetUserDeptName;
    /**
     * 内容
     */
    private String content;
    /**
     * 标题
     */
    private String title;
    /**
     * 消息类型Code
     */
    private String msgTypeCode = "MSG";
    /**
     * 消息类型Name
     */
    private String msgTypeName = "消息";
    /**
     * 弹出功能信息 json格式
     */
    private String showFuncFormInfo;

    public NoticeMsg(){

    }

    public NoticeMsg(String targetUserId, String targetUserName, String targetUserDeptId, String targetUserDeptName, String content, String title, String msgTypeCode, String msgTypeName) {
        this.targetUserId = targetUserId;
        this.targetUserName = targetUserName;
        this.targetUserDeptId = targetUserDeptId;
        this.targetUserDeptName = targetUserDeptName;
        this.content = content;
        this.title = title;
        this.msgTypeCode = msgTypeCode;
        this.msgTypeName = msgTypeName;
    }

    public NoticeMsg(String targetUserId, String targetUserName, String targetUserDeptId, String targetUserDeptName, String content, String title, String msgTypeCode, String msgTypeName, String showFuncFormInfo) {
        this(targetUserId, targetUserName, targetUserDeptId, targetUserDeptName, content, title, msgTypeCode, msgTypeName);
        this.showFuncFormInfo = showFuncFormInfo;
    }

    public String getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(String targetUserId) {
        this.targetUserId = targetUserId;
    }

    public String getTargetUserName() {
        return targetUserName;
    }

    public void setTargetUserName(String targetUserName) {
        this.targetUserName = targetUserName;
    }

    public String getTargetUserDeptId() {
        return targetUserDeptId;
    }

    public void setTargetUserDeptId(String targetUserDeptId) {
        this.targetUserDeptId = targetUserDeptId;
    }

    public String getTargetUserDeptName() {
        return targetUserDeptName;
    }

    public void setTargetUserDeptName(String targetUserDeptName) {
        this.targetUserDeptName = targetUserDeptName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMsgTypeCode() {
        return msgTypeCode;
    }

    public void setMsgTypeCode(String msgTypeCode) {
        this.msgTypeCode = msgTypeCode;
    }

    public String getMsgTypeName() {
        return msgTypeName;
    }

    public void setMsgTypeName(String msgTypeName) {
        this.msgTypeName = msgTypeName;
    }

    public String getShowFuncFormInfo() {
        return showFuncFormInfo;
    }

    public void setShowFuncFormInfo(String showFuncFormInfo) {
        this.showFuncFormInfo = showFuncFormInfo;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service.rpc;

import com.alibaba.fastjson2.JSONArray;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.document.InternalFileBO;
import com.je.common.base.document.FileBound;
import com.je.common.base.document.FileCopyDTO;
import com.je.common.base.document.InternalFileUpload;
import java.io.File;
import java.util.List;

/**
 * 文件内部服务操作
 */
public interface DocumentInternalRpcService {

    InternalFileBO saveSingleFile(InternalFileUpload fileUploadFile, File file, String userId);

    InternalFileBO saveSingleByteFile(InternalFileUpload fileUploadFile, byte[] fileContent, String userId);

    InternalFileBO saveSingleFileWithMetadata(InternalFileUpload fileUploadFile, File file, String userId, JSONObject metadata);

    InternalFileBO saveSingleFileWithBucket(InternalFileUpload fileUploadFile, File file, String userId, String bucket);

    InternalFileBO saveSingleFileWithMetadataAndBucket(InternalFileUpload fileUploadFile, File file, String userId, JSONObject metadata, String bucket);

    List<InternalFileBO> saveMultiFile(List<InternalFileUpload> fileUploadFiles, List<File> files, String userId);

    List<InternalFileBO> saveMultiFileWithMetadata(List<InternalFileUpload> fileUploadFiles, List<File> files, String userId, JSONObject metadata);

    List<InternalFileBO> saveMultiFileWithBucket(List<InternalFileUpload> fileUploadFiles, List<File> files, String userId, String bucket);

    List<InternalFileBO> saveMultiFileWithMetadataAndBucket(List<InternalFileUpload> fileUploadFiles, List<File> files, String userId, JSONObject metadata, String bucket);

    List<InternalFileBO> saveMultiFileWithMetadataAndBucketToDestDir(List<InternalFileUpload> fileUploadFiles, List<File> files, String userId, JSONObject metadata, String bucket, String dir);

    InternalFileBO boundSingleFile(FileBound fileBoundFile, String userId, JSONObject metadata, String bucket);

    List<InternalFileBO> boundFile(List<FileBound> fileBounds, String userId, JSONObject metadata, String bucket);

    void saveFileMetadataWithFileKeys(String userId, JSONObject metadata, List<String> fileKeys);

    void saveFileMetadataWithRelIdAndFileId(String userId, JSONObject metadata, String relId, String fileId);

    void updateMetadataByKey(String fileKey, String newRelName, JSONObject metadata, String userId);

    JSONObject selectFileMetadataByKey(String fileKey);

    InternalFileBO editByFileKey(String fileKey, File file, String userId);

    InternalFileBO copyRelByKey(String fileKey, String newRelName, JSONObject metadata, String userId);

    void delFilesByKey(List<String> fileKeys, String userId);

    boolean exists(String fileKey);

    InternalFileBO selectFileByKey(String fileKey);

    InternalFileBO selectFileByKeyAndVersion(String fileKey, String version);

    List<InternalFileBO> selectFileListByKey(List<String> keys);

    List<InternalFileBO> selectFileByMetadata(JSONObject metadataQuery);

    List<InternalFileBO> selectFileFullByMetadata(JSONObject metadataQuery);

    File readFile(String fileKey);

    List<InternalFileBO> selectAllVersionFiles(String fikeKey);

    InternalFileBO selectVersionFile(String fikeKey, int version);

    void updateFiledFile(String userId, String tableCode, String pkValue, String fieldCode, List<String> usedFileKey);

    List<FileCopyDTO> copyFiles(String userId, String tableCode, String pkValue, String fieldCode, String newPkValue);

    List<FileCopyDTO> copyFilesWithFuncCode(String userId, String tableCode, String pkValue, String fieldCode, String newPkValue, String newFuncCode, String newTableCode, String newFiledCode);

    void deleteFiles(String tableCode, String pkValues, String userId);

    void deleteFilesWithFieldCodes(String tableCode, String pkValues, String fieldCodes, String userId);

    void deleteFilesWithFieldCodesAndUploadTypes(String tableCode, String pkValues, String fieldCodes, String uploadTypes, String userId);

    JSONArray listFilesMeta(String bucket, String path);

    File readThumbnail(String fileKey );

    void deleteByFileKeys(List<String> fileKeys, String userId);

}

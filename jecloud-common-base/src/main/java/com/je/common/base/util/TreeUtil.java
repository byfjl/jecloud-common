/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.google.common.base.Strings;
import com.je.core.entity.extjs.JSONTreeNode;
import com.je.common.base.constants.tree.NodeType;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 树形节点工具类
 *
 * @author zhangshuaipeng
 */
public class TreeUtil {
    /**
     * 构建根节点
     *
     * @return
     */
    public static JSONTreeNode buildRootNode() {
        return buildTreeNode(NodeType.ROOT, NodeType.ROOT, NodeType.ROOT, "", "", "", null);
    }

    /**
     * 构建结构性返回的树形VO
     *
     * @param id           主键
     * @param text         展示名称 就是要展示出来的内容
     * @param code         编码 没有空
     * @param nodeInfo     节点信息 没有空
     * @param nodeInfoType 节点信息类型 没有空
     * @param icon         图标
     * @return
     */
    public static JSONTreeNode buildTreeNode(String id, String text, String code, String nodeInfo, String nodeInfoType, String icon, String parent) {
        JSONTreeNode node = new JSONTreeNode();
        node.setId(id);
        node.setText(text);
        node.setCode(code);
        node.setNodeInfo(nodeInfo);
        node.setNodeInfoType(nodeInfoType);
        node.setDisabled("1");
        node.setIcon(icon);
        node.setParent(parent);
        node.setLeaf(false);
        return node;
    }

    public static JSONTreeNode copyNewTreeNode(JSONTreeNode node) {
        JSONTreeNode n = new JSONTreeNode();
        n.setBean(new HashMap<String, Object>(node.getBean()));
        n.setChecked(node.getChecked());
        n.setChildren(new ArrayList<JSONTreeNode>());
        n.setCode(node.getCode());
        n.setDescription(node.getDescription());
        n.setDisabled(node.getDisabled());
        n.setExpandable(node.isExpandable());
        n.setExpanded(node.getExpanded());
        n.setIcon(node.getIcon());
        n.setIconColor(node.getIconColor());
        n.setId(node.getId());
        n.setLayer(node.getLayer());
        n.setLeaf(node.isLeaf());
        n.setNodeInfo(node.getNodeInfo());
        n.setNodeInfoType(node.getNodeInfoType());
        n.setNodePath(node.getNodePath());
        n.setNodeType(node.getNodeType());
        n.setOrderIndex(node.getOrderIndex());
        n.setParent(node.getParent());
        n.setText(node.getText());
        n.setTreeOrderIndex(node.getTreeOrderIndex());
        return n;
    }

    public static boolean verify(JSONTreeNode template) {
        if (!Strings.isNullOrEmpty(template.getId())) {
            if (!Strings.isNullOrEmpty(template.getCode())) {
                if (!Strings.isNullOrEmpty(template.getText())) {
                    if (!Strings.isNullOrEmpty(template.getParent())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
}

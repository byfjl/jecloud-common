/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import java.util.function.Supplier;

/**
 * 异常捕获类
 *
 * @author wangmm
 * @date 2021/1/27
 */
public class CatchUtil {

    public static <T> T doCatch(Supplier<T> supplier) {
        return doCatch(null, supplier);
    }

    public static <T> T doCatch(String message, Supplier<T> supplier) {
        return doCatch(null, PlatformExceptionEnum.UNKOWN_ERROR, supplier);
    }

    /**
     * @param message       错误信息
     * @param exceptionEnum 错误类型
     * @param supplier      执行函数
     * @param <T>           返回值类型
     * @return supplier.get()执行结果
     */
    public static <T> T doCatch(String message, PlatformExceptionEnum exceptionEnum, Supplier<T> supplier) {
        try {
            return supplier.get();
        } catch (Exception e) {
            throw new PlatformException(message == null ? e.getMessage() : message, exceptionEnum, e);
        }
    }

}
/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class XmlUtils {
	/**
	 * 加载xml文档文件
	 * @param filePath
	 * @return
	 */
	public static Document loadXMLFile(String filePath){
         SAXReader saxReader = new SAXReader();
         try {
			return saxReader.read(new File(filePath));
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
     }	
	 public static boolean saveFormatXMLDocumentToFile(Document doc, String encoding, String filePath)  {
      boolean flag = false;
      OutputFormat outputFormat = OutputFormat.createPrettyPrint();
       outputFormat.setEncoding(encoding);
       FileOutputStream fos = null;
	        XMLWriter xmlWriter  = null;
	          try{
	              fos = new FileOutputStream(filePath);// 可解决UTF-8编码问题
	              xmlWriter = new XMLWriter(fos, outputFormat);
	              xmlWriter.write(doc);
	              flag = true;
				  xmlWriter.flush();
				  fos.flush();
	          }catch(IOException e){
	             throw new PlatformException("XML工具类读取文件信息异常", PlatformExceptionEnum.JE_CORE_UTIL_XML_READ_ERROR,new Object[]{filePath},e);
	          }finally{
	              try {
					  if (xmlWriter != null) {
						  xmlWriter.close();
					  }
					  if (fos != null) {
						  fos.close();
					  }
				  } catch (IOException e) {
	                  throw new PlatformException("XML工具类关闭文件流异常",PlatformExceptionEnum.JE_CORE_UTIL_XML_CLOSEIO_ERROR,new Object[]{},e);
	              }
	              
	          }
	          return flag;
	      }
}

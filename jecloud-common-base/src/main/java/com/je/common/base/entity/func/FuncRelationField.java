/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.entity.func;

import java.io.Serializable;

public class FuncRelationField implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = -1248650493234925057L;
    /**
     * 主功能字段编码
     */
    private String fieldCode;
    /**
     * 子功能字段编码
     */
    private String childFieldCode;
    /**
     * 自定义SQL
     */
    private String diySql;
    /**
     * 关联关系
     */
    private String whereType;
    /**
     * 传值
     */
    private Boolean doValue;
    /**
     * WHERE条件
     */
    private Boolean whereChild;
    /**
     * 级联删除子功能
     */
    private Boolean deleteChild;
    /**
     * 级联更新
     */
    private Boolean updateChild;

    public String getFieldCode() {
        return fieldCode;
    }

    public void setFieldCode(String fieldCode) {
        this.fieldCode = fieldCode;
    }

    public String getChildFieldCode() {
        return childFieldCode;
    }

    public void setChildFieldCode(String childFieldCode) {
        this.childFieldCode = childFieldCode;
    }

    public String getDiySql() {
        return diySql;
    }

    public void setDiySql(String diySql) {
        this.diySql = diySql;
    }

    public String getWhereType() {
        return whereType;
    }

    public void setWhereType(String whereType) {
        this.whereType = whereType;
    }

    public Boolean getDoValue() {
        return doValue;
    }

    public void setDoValue(Boolean doValue) {
        this.doValue = doValue;
    }

    public Boolean getWhereChild() {
        return whereChild;
    }

    public void setWhereChild(Boolean whereChild) {
        this.whereChild = whereChild;
    }

    public Boolean getDeleteChild() {
        return deleteChild;
    }

    public void setDeleteChild(Boolean deleteChild) {
        this.deleteChild = deleteChild;
    }

    public Boolean getUpdateChild() {
        return updateChild;
    }

    public void setUpdateChild(Boolean updateChild) {
        this.updateChild = updateChild;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.func.funcPerm.fastAuth;

import java.io.Serializable;

/**
 * 快速授权
 */
public class FastAuthVo implements Serializable {
    /**
     * 授权开关
     */
    private String authOnOff;
    /**
     * 授权code字符串，用逗号分隔
     */
    private String selectPermStr;
    /**
     * 部门控制
     */
    private FastAuthScopeVo deptControl;
    /**
     * 机构控制
     */
    private FastAuthScopeVo orgControl;
    /**
     * 角色控制
     */
    private FastAuthScopeVo roleControl;
    /**
     * 人员控制
     */
    private FastAuthScopeVo userControl;

    public String getSelectPermStr() {
        return selectPermStr;
    }

    public void setSelectPermStr(String selectPermStr) {
        this.selectPermStr = selectPermStr;
    }

    public String getAuthOnOff() {
        return authOnOff;
    }

    public void setAuthOnOff(String authOnOff) {
        this.authOnOff = authOnOff;
    }

    public FastAuthScopeVo getDeptControl() {
        return deptControl;
    }

    public void setDeptControl(FastAuthScopeVo deptControl) {
        this.deptControl = deptControl;
    }

    public FastAuthScopeVo getOrgControl() {
        return orgControl;
    }

    public void setOrgControl(FastAuthScopeVo orgControl) {
        this.orgControl = orgControl;
    }

    public FastAuthScopeVo getRoleControl() {
        return roleControl;
    }

    public void setRoleControl(FastAuthScopeVo roleControl) {
        this.roleControl = roleControl;
    }

    public FastAuthScopeVo getUserControl() {
        return userControl;
    }

    public void setUserControl(FastAuthScopeVo userControl) {
        this.userControl = userControl;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

/**
 * 系统属性
 *
 * @author wangmm@ketr.com.cn
 * @date 2020/8/31
 */
public class SystemProperty {

    /**
     * 是否是jar启动
     */
    public static final String IS_JAR = "je.isJar";
    /**
     * 外置根目录，存储临时文件等
     */
    public static final String ROOT_PATH = "je.path.root";
    /**
     * 自定义 license 地址
     */
    public static final String LICENSE_PATH = "je.path.license";


    public static String getRootPath() {
        return System.getProperty(ROOT_PATH);
    }

    public static void setRootPath(String value) {
        System.setProperty(ROOT_PATH, value.endsWith("/") ? value : (value + "/"));
        System.setProperty("jeplus.webapp", getRootPath());
    }

    public static String getLicensePath() {
        return System.getProperty(LICENSE_PATH);
    }

    public static void setLicensePath(String value) {
        System.setProperty(LICENSE_PATH, value);
    }

    public static boolean isJar() {
        return "1".equals(System.getProperty(IS_JAR));
    }

    public static void isJar(boolean value) {
        System.setProperty(IS_JAR, value ? "1" : "0");
    }

}

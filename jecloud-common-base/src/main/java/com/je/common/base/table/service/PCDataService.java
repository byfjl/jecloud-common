/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table.service;

import com.je.common.base.db.DbFieldVo;
import com.je.common.base.entity.extjs.DbIndex;
import com.je.common.base.entity.extjs.DbModel;
import com.je.common.base.entity.extjs.Model;
import java.util.List;

/**
 * 加载表跟SQL模型及结构
 */
public interface PCDataService {

    /**
	 * 加载存储过程字段模型
	 * @param dataSource 数据来源
	 * @param procedureName 程序名称
	 * @param fieldVos TODO 暂不明确
	 * @return
	 */
	List<Model> loadProcedure(String dataSource, String procedureName, List<DbFieldVo> fieldVos);

	/**
	 * 加载SQL字段模型
	 * @param dataSource 数据来源
	 * @param sql sql语句
	 * @param fieldVos TODO 暂不明确
	 * @return
	 */
	List<Model> loadSql(String dataSource, String sql, List<DbFieldVo> fieldVos);

	/**
	 * 加载指定表名数据库字段模型
	 * @param tableCode 表code
	 * @return
	 */
	List<DbModel> loadTableColumn(String tableCode);

	/**
	 * 加载指定表名数据库字段模型
	 * @param tableCode 表code
	 * @return
	 */
	List<DbModel> loadTableColumnBySql(String tableCode);

	/**
	 * 查询出表中的索引
	 * @param tableCode 表code
	 * @return
	 */
	List<DbIndex> loadTableIndex(String tableCode);

	/**
	 * 加载数据库所有索引名称(不支持MySQL)
	 * @return
	 */
	List<String> loadDbIndex();

	/**
	 *TODO 暂不明确
	 * @param tableCode
	 * @return
	 */
	Boolean existsTable(String tableCode);

	/**
	 * 构建存储过程占位参数
	 * @param params
	 * @return
	 */
	String getCallParams(Object[] params);


}

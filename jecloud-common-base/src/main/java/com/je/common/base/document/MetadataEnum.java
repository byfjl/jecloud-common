/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.document;

/**
 * 保存类型枚举
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/28
 */
public enum MetadataEnum {

    /*------------------------业务信息----------------------*/

    /**
     * 功能编码
     */
    funcCode("funcCode"),
    /**
     * 表名
     */
    tableCode("tableCode"),
    /**
     * 业务主键
     */
    pkValue("pkValue"),
    /**
     * 字段名
     */
    fieldCode("fieldCode"),
    /**
     * 字典ID 附件子功能使用
     */
    dicId("dicId"),
    /**
     * 字典code 附件子功能使用
     */
    dicCode("dicCode"),
    /**
     * 字典名称 附件子功能使用
     */
    dicName("dicName"),
    /**
     * 附件备注 附件子功能使用
     */
    remarks("remarks"),
    /**
     * 上传类型 UploadTypeEnum (FORM,CKEDITOR,SUBFUNC,NETDISK,TEMP,OTHER)
     */
    uploadType("uploadType"),
    /**
     * 业务类型BusTypeEnum (PLATFORM,PROJECT)  兼容老数据使用已废弃
     */
    @Deprecated
    busType("busType"),

    /*------------------------创建信息----------------------*/

    /**
     * 创建人姓名
     */
    createUserName("createUserName"),
    /**
     * 创建人部门ID
     */
    createUserDept("createUserDept"),
    /**
     * 创建人部门名称
     */
    createUserDeptName("createUserDeptName"),
    /**
     * 租户ID
     */
    tenantId("tenantId"),

    /*------------------------文件信息----------------------*/

    /**
     * 文件名称
     */
    relName("relName"),
    /**
     * 文件后缀
     */
    suffix("suffix", false),
    /**
     * 文件大小
     */
    size("size", false),
    /**
     * 文件存储空间
     */
    bucket("bucket", false),
    /**
     * 文件唯一标识
     */
    fileKey("fileKey", false),
    /**
     * 创建人
     */
    createUser("createUser"),
    /**
     * 创建时间
     */
    createTime("createTime"),
    /**
     * 修改人
     */
    modifiedUser("modifiedUser"),
    /**
     * 修改时间
     */
    modifiedTime("modifiedTime");

    /**
     * 元数据code
     */
    private String code;
    /**
     * 是否允许编辑
     */
    private boolean update;

    private MetadataEnum(String code) {
        this.code = code;
        this.update = true;
    }
    private MetadataEnum(String code, boolean update) {
        this.code = code;
        this.update = update;
    }

    public static MetadataEnum getDefault(String code) {
        if (code == null || "".equals(code)) {
            return null;
        }
        for (MetadataEnum value : MetadataEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return null;
    }

    public String getCode() {
        return code;
    }
    public boolean getUpdate() {
        return update;
    }

}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.controller;

import com.google.common.base.Strings;
import com.je.common.base.cache.AbstractHashCache;
import com.je.common.base.cache.AbstractListCache;
import com.je.common.base.mvc.AbstractPlatformController;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.spring.SpringContextHolder;
import org.apache.servicecomb.registry.DiscoveryManager;
import org.apache.servicecomb.registry.api.registry.Microservice;
import org.apache.servicecomb.registry.api.registry.MicroserviceInstance;
import org.apache.servicecomb.registry.consumer.MicroserviceManager;
import org.apache.servicecomb.registry.consumer.MicroserviceVersion;
import org.apache.servicecomb.registry.consumer.MicroserviceVersions;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/je/common/cache/manage")
public class CacheManagerController extends AbstractPlatformController {

    @RequestMapping(value = "/clearAll", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    public BaseRespResult clearAll(HttpServletRequest request){
        String cacheKey = getStringParameter(request,"cacheKey");
        String service = getStringParameter(request,"service");

        if(Strings.isNullOrEmpty(cacheKey)){
            return BaseRespResult.errorResult("请确认缓存Key！");
        }

        if(Strings.isNullOrEmpty(service)){
            return BaseRespResult.errorResult("请确认缓存服务名！");
        }

        Object object = SpringContextHolder.getBean(cacheKey);
        if(object == null){
            return BaseRespResult.errorResult("找不到此缓存！");
        }
        if (object instanceof AbstractHashCache) {
            AbstractHashCache hashCache = (AbstractHashCache) object;
            hashCache.clear();
        } else if (object instanceof AbstractListCache) {
            AbstractListCache listCache = (AbstractListCache) object;
            listCache.clear();
        }
        return BaseRespResult.successResult("清除缓存成功！");
    }

    @RequestMapping(value = {"/contract/remove"}, method = RequestMethod.GET, produces = "application/json; charset=utf-8",
            name = "清理契约缓存")
    public void remove(HttpServletRequest request, String microKey) {
        Map<String, MicroserviceManager> map = DiscoveryManager.INSTANCE.getAppManager().getApps();
        for (String microservice : map.keySet()) {
            MicroserviceManager microserviceManager = map.get(microservice);
            Map<String, MicroserviceVersions> microserviceVersionsMap = microserviceManager.getVersionsByName();
            if (Strings.isNullOrEmpty(microKey) || microKey.equals("all")) {
                for (String microserviceVersionKey : microserviceVersionsMap.keySet()) {
                    microserviceManager.getVersionsByName().remove(microserviceVersionKey);
                }
                return;
            }
            microserviceManager.getVersionsByName().remove(microKey);
        }
    }

}

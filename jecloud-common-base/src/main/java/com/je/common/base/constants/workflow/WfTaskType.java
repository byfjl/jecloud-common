/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.workflow;

public class WfTaskType {
	/**
	 * 开始节点 
	 */
	public static final String NODE_START = "start";
	/**
	 * 任务节点
	 */
	public static final String NODE_TASK = "task";
	/**
	 * 判断节点
	 */
	public static final String NODE_DECISION = "decision";
	/**
	 * 分支节点
	 */
	public static final String NODE_FORK = "fork";
	/**
	 * 聚合节点
	 */
	public static final String NODE_JOIN = "join";
	/**
	 *  结束节点
	 */
	public static final String NODE_END = "end";
	/**
	 * 候选节点
	 */
	public static final String NODE_JOINT = "joint";
	/**
	 * 多人处理节点
	 */
	public static final String NODE_BATCHTASK = "batchtask";
	/**
	 * 会签节点
	 */
	public static final String NODE_COUNTERSIGN = "countersign";
	/**
	 * 固定执行人节点
	 */
	public static final String NODE_TO_ASSIGNEE = "to_assignee";
	/**
	 * 自动节点
	 */
	public static final String NODE_AUTO = "auto";
	/**
	 * 循环流
	 */
	public static final String NODE_CIRCULAR="circular";
	/**
	 * 中转节点， 来作为转办和委托的时候进行的任务节点名称
	 */
	public static final String NODE_ENTRUST_TRANSMIT_TASK="ENTRUST_TRANSMIT_TASK";
	/**
	 * 强制预定义节点
	 */
	public static final String USERDIY_TASK="USERDIY_TASK";
	/**
	 * 跳跃中转节点
	 */
	public static final String JUMP_TASK="JUMP_TASK";
	/**
	 * 调拨中转节点
	 */
	public static final String ALLOT_TASK="ALLOT_TASK";
	/**
	 * 传阅类型
	 */
	public static final String ROUND = "ROUND";
}

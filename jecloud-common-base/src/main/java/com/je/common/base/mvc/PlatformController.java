/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.mvc;

import com.google.common.base.Strings;
import com.je.common.base.result.BaseRespResult;
import com.je.common.base.util.StringUtil;
import org.apache.servicecomb.swagger.invocation.context.ContextUtils;
import org.apache.servicecomb.swagger.invocation.context.InvocationContext;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

/**
 * @program: jecloud-common
 * @author: LIULJ
 * @create: 2021-03-12 08:58
 * @description:
 */
public interface PlatformController {

    default String getStringParameter(HttpServletRequest request, String key) {
        if (StringUtil.isNotEmpty(request.getParameter(key))) {
            return URLDecoder.decode(request.getParameter(key));
        } else {
            return request.getParameter(key);
        }
    }

    default Object getObjectParamter(HttpServletRequest request, String key) {
        if (request.getParameterMap().containsKey(key)) {
            return request.getParameter(key);
        } else {
            return request.getAttribute(key);
        }
    }

    default String getStringParameterByContext(String key) {
        if (Strings.isNullOrEmpty(key)) {
            return null;
        }
        InvocationContext invocationContext = ContextUtils.getInvocationContext();
        String value = null;
        try {
            if(invocationContext.getContext(key) != null){
                value = URLDecoder.decode(invocationContext.getContext(key), StandardCharsets.UTF_8.name());
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return value;
    }

    default Object getObjectParameterByContext(String key) {
        if (Strings.isNullOrEmpty(key)) {
            return null;
        }
        InvocationContext invocationContext = ContextUtils.getInvocationContext();
        Object value = null;
        try {
            if(invocationContext.getContext(key) != null){
                value = URLDecoder.decode(invocationContext.getContext(key), StandardCharsets.UTF_8.name());
            }
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        return value;
    }

    /**
     * 默认的列表读取
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult load(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 默认平台保存
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult doSave(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 默认平台复制
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult doCopy(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 默认平台修改
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult doUpdate(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 默认平台删除(真实删除)
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult doRemove(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 默认平台禁用
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult doDisable(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 默认平台启用
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult doEnable(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 默认平台列表修改
     * 列表批量修改，列表批量添加接口
     * 添加的时候特殊标识
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult doUpdateList(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 根据主键获取
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult getInfoById(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 验证唯一性
     *
     * @param param 请求参数封装对象
     */
    BaseRespResult checkFieldUniquen(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 统计
     *
     * @param param
     * @param request
     * @return
     */
    public BaseRespResult statistics(BaseMethodArgument param, HttpServletRequest request);

    /**
     * 批量修改列表数据
     * @param param
     * @param request
     * @return
     */
    BaseRespResult batchModifyListData (BaseMethodArgument param, HttpServletRequest request);


}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.func.funcPerm.FieldAuth;


import java.io.Serializable;

public class FieldAuthVo implements Serializable {
    /**
     * 授权开关
     */
    private String authOnOff;
    /**
     * 授权范围
     */
    private String authScope;

    /**
     * 角色授权
     */
    private FieldAuthScopeVo roleAuthFieldVo;
    /**
     * 部门授权
     */
    private FieldAuthScopeVo deptAuthFieldVo;

    /**
     * 机构授权
     */
    private FieldAuthScopeVo orgAuthFieldVo;


    public String getAuthOnOff() {
        return authOnOff;
    }

    public void setAuthOnOff(String authOnOff) {
        this.authOnOff = authOnOff;
    }

    public String getAuthScope() {
        return authScope;
    }

    public void setAuthScope(String authScope) {
        this.authScope = authScope;
    }

    public FieldAuthScopeVo getRoleAuthFieldVo() {
        return roleAuthFieldVo;
    }

    public void setRoleAuthFieldVo(FieldAuthScopeVo roleAuthFieldVo) {
        this.roleAuthFieldVo = roleAuthFieldVo;
    }

    public FieldAuthScopeVo getDeptAuthFieldVo() {
        return deptAuthFieldVo;
    }

    public void setDeptAuthFieldVo(FieldAuthScopeVo deptAuthFieldVo) {
        this.deptAuthFieldVo = deptAuthFieldVo;
    }

    public FieldAuthScopeVo getOrgAuthFieldVo() {
        return orgAuthFieldVo;
    }

    public void setOrgAuthFieldVo(FieldAuthScopeVo orgAuthFieldVo) {
        this.orgAuthFieldVo = orgAuthFieldVo;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.log.factory.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.log.factory.SelectService;
import com.je.ibatis.extension.conditions.ConditionsWrapper;
import com.je.ibatis.extension.plugins.pagination.Page;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;

import java.util.HashMap;

public class SelectSqlServiceImpl implements SelectService {

    private static final HashMap<String,String> map = new HashMap();

    static {
        map.put("wrapper","selectSql1p");
        map.put("sql@params","selectSql2p");
        map.put("page@wrapper","selectSqlByPage2p");
        map.put("current@size@sql@params","selectSqlByPage4p");
    }

    @Override
    public String getSql(ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        String methodType = getMethodType(signature.getMethod(),map);
        JSONObject jsonObject = new JSONObject();
        switch (methodType){
            case "selectSql2p":
                jsonObject =   selectSql2p(joinPoint.getArgs());
                break;
            case "selectSqlByPage2p":
                jsonObject =    selectSqlByPage2p(joinPoint.getArgs());
                break;
            case "selectSqlByPage4p" :
                jsonObject =    selectSqlByPage4p(joinPoint.getArgs());
                break;
            case "selectSql1p" :
                jsonObject =    selectSql1p(joinPoint.getArgs());
                break;
        }
        return   JSON.toJSONString(jsonObject);
    }

    private JSONObject selectSql1p(Object[] args) {
        if(args==null||args.length!=1){
            return null;
        }
        if(args[0] instanceof ConditionsWrapper){
            ConditionsWrapper conditionsWrapper = (ConditionsWrapper) args[0];
            return getSqlInfoByWrapper(conditionsWrapper);
        }
        return null;
    }

    private JSONObject selectSqlByPage2p(Object[] args) {
        if(args==null||args.length!=2){
            return null;
        }

        if(args[1] instanceof ConditionsWrapper){
            JSONObject jsonObject = new JSONObject();
            ConditionsWrapper conditionsWrapper = (ConditionsWrapper) args[1];
            String text = conditionsWrapper.getParameterSql();
            if(args[0] instanceof Page){
                Page page = (Page) args[0];
                text = applySqlByPage(page,text);
            }
            jsonObject.put(sql_text,text);
            jsonObject.put(sql_parameter,"");
            return jsonObject;
        }
        return null;
    }

    private JSONObject selectSqlByPage4p(Object[] args) {
        if(args==null||args.length!=4){
            return null;
        }

        JSONObject jsonObject = new JSONObject();
        String sql = args[2].toString();
        Object[] arr = (Object[]) args[3];
        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
        conditionsWrapper.apply(sql,arr);
        String text = conditionsWrapper.getParameterSql();

        int current= (Integer) args[0];
        int size = (Integer) args[1];
        if(current!=-1&&size!=-1 ){
            long startLine = current==0?0:(current-1)*size;
            text+=" LIMIT "+startLine+","+size;
        }
        jsonObject.put(sql_text,text);
        jsonObject.put(sql_parameter,"");
        return jsonObject;
    }

    private JSONObject selectSql2p(Object[] objects) {
        if(objects==null||objects.length!=2){
            return null;
        }

        String sql = (String) objects[0];
        Object[] arr = (Object[]) objects[1];
        ConditionsWrapper conditionsWrapper = ConditionsWrapper.builder();
        conditionsWrapper.apply(sql,arr);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(sql_text,conditionsWrapper.getParameterSql());
        jsonObject.put(sql_parameter,"");
        return jsonObject;
    }

    private JSONObject getParameterByObjectArr(Object[] arg) {
        if(arg==null|| arg.length==0){
            return null;
        }
        JSONObject jsonObject = new JSONObject();
        for(int i =0;i<arg.length;i++){
            jsonObject.put("("+i+")",arg[i]);
        }

        return jsonObject;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.message;

public class WxMessageType {
	/**
	 * 文字
	 */
	public static final String WZ = "WZ";
	/**
	 * 文字
	 */
	public static final String KP = "KP";
	/**
	 * OA
	 */
	public static final String OA = "OA";
	/**
	 * MD
	 */
	public static final String MD = "MD";
	/**
	 * 链接
	 */
	public static final String LJ = "LJ";
	/**
	 * 图文
	 */
	public static final String TW = "TW";
	/**
	 * 图片
	 */
	public static final String TP = "TP";
	/**
	 * 语音
	 */
	public static final String YY = "YY";
	/**
	 * 视频
	 */
	public static final String SP = "SP";
	/**
	 * 音乐
	 */
	public static final String YINY = "YINY";
	/**
	 * 文件
	 */
	public static final String WJ = "WJ";
}

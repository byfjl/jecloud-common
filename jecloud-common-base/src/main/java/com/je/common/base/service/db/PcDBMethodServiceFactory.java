/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service.db;

import com.je.common.base.constants.ConstantVars;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.service.db.PcDBMethodService;
import com.je.common.base.spring.SpringContextHolder;

/**
 * @program: jecloud-common
 * @author: LIULJ
 * @create: 2021-03-03 15:19
 * @description:
 */
public class PcDBMethodServiceFactory {

    public static PcDBMethodService getPcDBMethodService(){
        PcDBMethodService pcDBMethodService = null;
        if(JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_ORACLE)){
            pcDBMethodService = SpringContextHolder.getBean("pcDBMethod4OracleService");
        }else if(JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_SQLSERVER)){
            pcDBMethodService = SpringContextHolder.getBean("pcDBMethod4SqlServerService");
        }else if(JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_MYSQL)){
            pcDBMethodService = SpringContextHolder.getBean("pcDBMethod4MySqlService");
        }else if(JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_TIDB)){
            pcDBMethodService = SpringContextHolder.getBean("pcDBMethod4MySqlService");
        }else if(JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_SHENTONG)){
            pcDBMethodService = SpringContextHolder.getBean("pcDBMethod4OscarService");
        }else if(JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_KINGBASEES)){
            pcDBMethodService = SpringContextHolder.getBean("pcDBMethod4KingbaseService");
        }else if(JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_DM)){
            pcDBMethodService = SpringContextHolder.getBean("pcDBMethod4DmService");
        }else{
            throw new RuntimeException("未知的数据库异常！");
        }
        return pcDBMethodService;
    }

}

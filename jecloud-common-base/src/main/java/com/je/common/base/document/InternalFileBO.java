/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.document;

import com.alibaba.fastjson2.JSONObject;

import javax.persistence.Column;
import java.util.Date;

/**
 * 文件读取返回业务对象
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/9/3
 */
public class InternalFileBO extends BasePojo {

    /*------------------------文件信息----------------------*/

    /**
     * 文件ID
     */
    private String fileId;

    /**
     * 业务关联ID
     */
    private String relId;

    /**
     * 文件名称(含后缀)
     */
    private String relName;

    /**
     * 文件后缀
     */
    private String suffix;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 文件存储路径
     */
    private String filePath;

    /**
     * 文件缩略图路径
     */
    private String thumbnail;

    /**
     * 文件所属存储空间
     */
    private String bucket;

    /**
     * 文件唯一标识
     */
    private String fileKey;

    /**
     * 文件访问地址(私有文件为null)
     */
    private String fullUrl;

    /**
     * 文件类型
     */
    private String contentType;

    /*------------------------创建信息----------------------*/

    /**
     * 上传人ID
     */
    private String createUser;

    /**
     * 上传人
     */
    private String createUserName;

    /**
     * 上传人部门
     */
    private String createUserDeptName;

    /**
     * 上传时间
     */
    private Date createTime;

    /**
     * 租户ID
     */
    private String tenantId;

    /**
     * 修改人
     */
    private String modifiedUser;

    /**
     * 修改时间
     */
    private String modifiedTime;

    /*------------------------业务信息----------------------*/

    /**
     * 功能编码
     */
    private String funcCode;
    /**
     * 表名
     */
    private String tableCode;
    /**
     * 业务主键
     */
    private String pkValue;
    /**
     * 字段名
     */
    private String fieldCode;
    /**
     * 字典ID 附件子功能使用
     */
    private String dicId;
    /**
     * 字典code 附件子功能使用
     */
    private String dicCode;
    /**
     * 字典名称 附件子功能使用
     */
    private String dicName;
    /**
     * 附件备注 附件子功能使用
     */
    private String remarks;
    /**
     * 上传类型 UploadTypeEnum (FORM,CKEDITOR,SUBFUNC,NETDISK,TEMP,OTHER)
     */
    private String uploadType;
    /**
     * 所有业务元数据
     */
    private JSONObject metadata;

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    /**
     * version版本
     */
    private Integer version;


    public JSONObject getMetadata() {
        return metadata;
    }

    public void setMetadata(JSONObject metadata) {
        this.metadata = metadata;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getRelId() {
        return relId;
    }

    public void setRelId(String relId) {
        this.relId = relId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    @Column(name = "file_size")
    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getRelName() {
        return relName;
    }

    public void setRelName(String relName) {
        this.relName = relName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getFileKey() {
        return fileKey;
    }

    public void setFileKey(String fileKey) {
        this.fileKey = fileKey;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    public String getFullUrl() {
        return fullUrl;
    }

    public void setFullUrl(String fullUrl) {
        this.fullUrl = fullUrl;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCreateUserDeptName() {
        return createUserDeptName;
    }

    public void setCreateUserDeptName(String createUserDeptName) {
        this.createUserDeptName = createUserDeptName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getModifiedUser() {
        return modifiedUser;
    }

    public void setModifiedUser(String modifiedUser) {
        this.modifiedUser = modifiedUser;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public void setFuncCode(String funcCode) {
        this.funcCode = funcCode;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

    public String getPkValue() {
        return pkValue;
    }

    public void setPkValue(String pkValue) {
        this.pkValue = pkValue;
    }

    public String getFieldCode() {
        return fieldCode;
    }

    public void setFieldCode(String fieldCode) {
        this.fieldCode = fieldCode;
    }

    public String getDicId() {
        return dicId;
    }

    public void setDicId(String dicId) {
        this.dicId = dicId;
    }

    public String getDicCode() {
        return dicCode;
    }

    public void setDicCode(String dicCode) {
        this.dicCode = dicCode;
    }

    public String getDicName() {
        return dicName;
    }

    public void setDicName(String dicName) {
        this.dicName = dicName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
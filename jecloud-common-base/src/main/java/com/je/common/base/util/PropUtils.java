/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import java.io.IOException;
import java.util.Properties;

/**
 * @author chenmeng
 * 2012-4-19 下午4:45:13
 */
public class PropUtils {

	private static final String NULL_PROP = "";
	/**
     * 根据一个key读取sysconfig.properties文件的值，如果读取失败则返回NULL_PROP字符串
     * @param key
     * @return
     */
    public static String get(String key) {
		try {
			Properties props = PropertiesLoaderUtils.loadAllProperties("sysconfig.properties");
			return props.getProperty(key, NULL_PROP);
		} catch (IOException e) {
			throw new PlatformException("系统配置文件工具类获取CONFIG的配置文件属性值异常", PlatformExceptionEnum.JE_CORE_UTIL_SYSPROP_CONFIG_ERROR,new Object[]{key},e);
		}

    }
    /**
     * 根据一个key读取sysconfig.properties文件的值，如果读取失败则返回NULL_PROP字符串
     * @param key
     * @return
     */
    public static String getJdbc(String key) {
    	try {
    		Properties props = PropertiesLoaderUtils.loadAllProperties("jdbc.properties");
    		return props.getProperty(key, NULL_PROP);
    	} catch (IOException e) {
    		throw new PlatformException("系统配置文件工具类获取JDBC的配置文件属性值异常", PlatformExceptionEnum.JE_CORE_UTIL_SYSPROP_JDBC_ERROR,new Object[]{key},e);
    	}

    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.func.funcPerm;

import com.je.common.base.func.funcPerm.FieldAuth.FieldAuthVo;
import com.je.common.base.func.funcPerm.dictionaryAuth.DictionaryAuthVo;
import com.je.common.base.func.funcPerm.fastAuth.FastAuthVo;
import com.je.common.base.func.funcPerm.roleSqlAuth.RoleSqlAuthVo;


import java.io.Serializable;

public class FuncDataPermVo implements Serializable {

    /**
     * 快速授权
     */
    private FastAuthVo fastAuthVo;
    /**
     * 字段授权
     */
    private FieldAuthVo fieldAuthVo;
    /**
     * 字典授权
     */
    private DictionaryAuthVo dictionaryAuthVo;
    /**
     * 角色sql授权
     */
    private RoleSqlAuthVo roleSqlAuthVo;
    /**
     * sql授权
     */
    private String sql;
    /**
     * 控制字段
     */
    private ControlFieldAuthVo controlFieldAuthVo;

    public FastAuthVo getFastAuthVo() {
        return fastAuthVo;
    }

    public void setFastAuthVo(FastAuthVo fastAuthVo) {
        this.fastAuthVo = fastAuthVo;
    }

    public FieldAuthVo getFieldAuthVo() {
        return fieldAuthVo;
    }

    public void setFieldAuthVo(FieldAuthVo fieldAuthVo) {
        this.fieldAuthVo = fieldAuthVo;
    }

    public DictionaryAuthVo getDictionaryAuthVo() {
        return dictionaryAuthVo;
    }

    public void setDictionaryAuthVo(DictionaryAuthVo dictionaryAuthVo) {
        this.dictionaryAuthVo = dictionaryAuthVo;
    }

    public RoleSqlAuthVo getRoleSqlAuthVo() {
        return roleSqlAuthVo;
    }

    public void setRoleSqlAuthVo(RoleSqlAuthVo roleSqlAuthVo) {
        this.roleSqlAuthVo = roleSqlAuthVo;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public ControlFieldAuthVo getControlFieldAuthVo() {
        return controlFieldAuthVo;
    }

    public void setControlFieldAuthVo(ControlFieldAuthVo controlFieldAuthVo) {
        this.controlFieldAuthVo = controlFieldAuthVo;
    }
}

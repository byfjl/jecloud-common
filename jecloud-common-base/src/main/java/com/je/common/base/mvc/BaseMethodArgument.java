/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.mvc;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.google.common.base.Strings;
import com.je.common.base.entity.QueryInfo;
import com.je.common.base.mapper.query.NativeQuery;
import com.je.common.base.mapper.query.Query;
import org.apache.commons.lang.StringUtils;
import java.util.Date;

public class BaseMethodArgument {

    //-------------------------------------------------基础参数

    /**
     * 功能ID
     */
    protected String funcId;
    /**
     * 功能code
     */
    protected String funcCode;
    /**
     * 表名
     */
    protected String tableCode;
    /**
     * 主键值
     */
    protected String pkValue;
    /**
     * 进行删除和选中操作时的ID列表，以逗号分隔
     */
    protected String ids;

    //-------------------------------------------------j_query参数

    protected String jQuery;

    protected String customQuery;

    protected String queryInfo;

    //-------------------------------------------------load参数

    /**
     * 开始位置
     */
    protected int start = 0;
    /**
     * 第几页
     */
    protected int page = 1;
    /**
     * 每页几条
     */
    protected int limit = 30;
    /**
     * 加载类型[子功能;查询选择字段;自定义数据源]
     */
    protected String loadType;
    /**
     * 父功能code
     */
    protected String parentFuncCode;
    /**
     * 表单值(作为查询选择字段whereSql参数)
     */
    protected String formData;
    /**
     * 自定义数据源ID
     */
    protected String datasourceId;


    protected String nowYear = (new Date().getYear() + 1900) + ""; //当前年

    protected String nowMonth = (new Date().getMonth() + 1) + ""; //当前月
    /**************************************辅助字段*****************************************/
    protected String modelName;//前台向后面传递的类的全名称
    protected String checked = "0"; // 1为checked 0为unchecked
    /**************************************VO字段*****************************************/
    protected String sort = "";//排序字段
    protected String whereSql = "";//查询条件
    protected String expandSql = "";//扩展查询条件
    protected String parentSql = "";//主子功能SQL
    protected String orderSql = ""; // 综合排序子句
    protected String useOrderSql = "0";//是否引用orderSql  解决功能分组排序和点击列排序 传参一样， 列排序不追加orderSql，分组追加
    protected String strData;//用于大数据资料的传输:大多是对象集合
    protected String queryColumns;//查询字段
    protected String sql = "";
    protected String node;
    protected String foreignKey = "";
    protected String codeGenFieldInfo = "";
    /**
     * 单附件字段
     */
    protected String uploadableFields = "";
    /**
     * 多附件字段
     */
    protected String batchFilesFields = "";
    /**
     * 树形字段
     */
    protected String rootId;
    protected Boolean onlyItem;
    protected String excludes = "";
    protected String parentId;//父节点主键
    protected int layer = 0;//所在的层数
    protected String nodeType;
    protected Boolean doTree = false; //是否树形操作
    protected Boolean doCombine = false;
    protected Boolean moreRoot = false;
    protected Boolean columnLazy = false;
    protected Boolean mark = false;
    protected Boolean funcEdit = false;
    protected Boolean postil = false;
    protected Boolean removeAll = false;
    protected Boolean initSys = false;
    protected String cascade;

    protected String queryType;
    protected String datasourceName;
    protected String procedureName;
    protected String queryParamsStr;
    protected String dbQueryObj;
    protected String dbSql;
    protected String zhId;
    protected String zhMc;
    protected String type;
    protected String code;
    protected String value;

    /***
     * 文档参数
     */
    protected String fileName;
    protected String realPath;
    protected String jeFileType;
    protected String jeFileSaveType;
    protected String contextType;
    protected String path;

    protected String otherParams = "";

    public String getLoadType() {
        return loadType;
    }

    public BaseMethodArgument setLoadType(String loadType) {
        this.loadType = loadType;
        return this;
    }

    public String getParentFuncCode() {
        return parentFuncCode;
    }

    public BaseMethodArgument setParentFuncCode(String parentFuncCode) {
        this.parentFuncCode = parentFuncCode;
        return this;
    }

    public String getFormData() {
        return formData;
    }

    public BaseMethodArgument setFormData(String formData) {
        this.formData = formData;
        return this;
    }

    public String getDatasourceId() {
        return datasourceId;
    }

    public BaseMethodArgument setDatasourceId(String datasourceId) {
        this.datasourceId = datasourceId;
        return this;
    }

    public Boolean getInitSys() {
        return initSys;
    }

    public BaseMethodArgument setInitSys(Boolean initSys) {
        this.initSys = initSys;
        return this;
    }

    public String getFuncId() {
        return funcId;
    }

    public BaseMethodArgument setFuncId(String funcId) {
        this.funcId = funcId;
        return this;
    }

    public String getDbSql() {
        return dbSql;
    }

    public BaseMethodArgument setDbSql(String dbSql) {
        this.dbSql = dbSql;
        return this;
    }

    public String getDatasourceName() {
        return datasourceName;
    }

    public BaseMethodArgument setDatasourceName(String datasourceName) {
        this.datasourceName = datasourceName;
        return this;
    }

    public String getProcedureName() {
        return procedureName;
    }

    public BaseMethodArgument setProcedureName(String procedureName) {
        this.procedureName = procedureName;
        return this;
    }

    public String getQueryParamsStr() {
        return queryParamsStr;
    }

    public BaseMethodArgument setQueryParamsStr(String queryParamsStr) {
        this.queryParamsStr = queryParamsStr;
        return this;
    }

    public String getDbQueryObj() {
        return dbQueryObj;
    }

    public BaseMethodArgument setDbQueryObj(String dbQueryObj) {
        this.dbQueryObj = dbQueryObj;
        return this;
    }

    public String getQueryType() {
        return queryType;
    }

    public BaseMethodArgument setQueryType(String queryType) {
        this.queryType = queryType;
        return this;
    }

    public String getNowYear() {
        return nowYear;
    }

    public BaseMethodArgument setNowYear(String nowYear) {
        this.nowYear = nowYear;
        return this;
    }

    public String getNowMonth() {
        return nowMonth;
    }

    public BaseMethodArgument setNowMonth(String nowMonth) {
        this.nowMonth = nowMonth;
        return this;
    }

    public String getTableCode() {
        return tableCode;
    }

    public BaseMethodArgument setTableCode(String tableCode) {
        this.tableCode = tableCode;
        return this;
    }

    public String getPkValue() {
        return pkValue;
    }

    public BaseMethodArgument setPkValue(String pkValue) {
        this.pkValue = pkValue;
        return this;
    }

    public String getModelName() {
        return modelName;
    }

    public BaseMethodArgument setModelName(String modelName) {
        this.modelName = modelName;
        return this;
    }

    public String getChecked() {
        return checked;
    }

    public BaseMethodArgument setChecked(String checked) {
        this.checked = checked;
        return this;
    }

    public int getStart() {
        return start;
    }

    public BaseMethodArgument setStart(int start) {
        this.start = start;
        return this;
    }

    public int getPage() {
        return page;
    }

    public BaseMethodArgument setPage(int page) {
        this.page = page;
        return this;
    }

    public int getLimit() {
        return limit;
    }

    public BaseMethodArgument setLimit(int limit) {
        this.limit = limit;
        return this;
    }

    public String getSort() {
        return sort;
    }

    public BaseMethodArgument setSort(String sort) {
        this.sort = sort;
        return this;
    }

    public String getWhereSql() {
        return whereSql;
    }

    public BaseMethodArgument setWhereSql(String whereSql) {
        this.whereSql = whereSql;
        return this;
    }

    public String getExpandSql() {
        return expandSql;
    }

    public BaseMethodArgument setExpandSql(String expandSql) {
        this.expandSql = expandSql;
        return this;
    }

    public String getParentSql() {
        return parentSql;
    }

    public BaseMethodArgument setParentSql(String parentSql) {
        this.parentSql = parentSql;
        return this;
    }

    public String getOrderSql() {
        return orderSql;
    }

    public BaseMethodArgument setOrderSql(String orderSql) {
        this.orderSql = orderSql;
        return this;
    }

    public String getUseOrderSql() {
        return useOrderSql;
    }

    public BaseMethodArgument setUseOrderSql(String useOrderSql) {
        this.useOrderSql = useOrderSql;
        return this;
    }

    public String getStrData() {
        return strData;
    }

    public BaseMethodArgument setStrData(String strData) {
        this.strData = strData;
        return this;
    }

    public String getQueryInfo() {
        return queryInfo;
    }

    public BaseMethodArgument setQueryInfo(String queryInfo) {
        this.queryInfo = queryInfo;
        return this;
    }

    public String getQueryColumns() {
        return queryColumns;
    }

    public BaseMethodArgument setQueryColumns(String queryColumns) {
        this.queryColumns = queryColumns;
        return this;
    }

    public String getIds() {
        return ids;
    }

    public BaseMethodArgument setIds(String ids) {
        this.ids = ids;
        return this;
    }

    public String getSql() {
        return sql;
    }

    public BaseMethodArgument setSql(String sql) {
        this.sql = sql;
        return this;
    }

    public String getNode() {
        return node;
    }

    public BaseMethodArgument setNode(String node) {
        this.node = node;
        return this;
    }

    public String getForeignKey() {
        return foreignKey;
    }

    public BaseMethodArgument setForeignKey(String foreignKey) {
        this.foreignKey = foreignKey;
        return this;
    }

    public String getFuncCode() {
        return funcCode;
    }

    public BaseMethodArgument setFuncCode(String funcCode) {
        this.funcCode = funcCode;
        return this;
    }

    public String getCodeGenFieldInfo() {
        return codeGenFieldInfo;
    }

    public BaseMethodArgument setCodeGenFieldInfo(String codeGenFieldInfo) {
        this.codeGenFieldInfo = codeGenFieldInfo;
        return this;
    }

    public String getUploadableFields() {
        return uploadableFields;
    }

    public BaseMethodArgument setUploadableFields(String uploadableFields) {
        this.uploadableFields = uploadableFields;
        return this;
    }

    public String getBatchFilesFields() {
        return batchFilesFields;
    }

    public BaseMethodArgument setBatchFilesFields(String batchFilesFields) {
        this.batchFilesFields = batchFilesFields;
        return this;
    }

    public String getRootId() {
        return rootId;
    }

    public BaseMethodArgument setRootId(String rootId) {
        this.rootId = rootId;
        return this;
    }

    public Boolean getOnlyItem() {
        return onlyItem;
    }

    public BaseMethodArgument setOnlyItem(Boolean onlyItem) {
        this.onlyItem = onlyItem;
        return this;
    }

    public String getExcludes() {
        return excludes;
    }

    public BaseMethodArgument setExcludes(String excludes) {
        this.excludes = excludes;
        return this;
    }

    public String getParentId() {
        return parentId;
    }

    public BaseMethodArgument setParentId(String parentId) {
        this.parentId = parentId;
        return this;
    }

    public int getLayer() {
        return layer;
    }

    public BaseMethodArgument setLayer(int layer) {
        this.layer = layer;
        return this;
    }

    public String getNodeType() {
        return nodeType;
    }

    public BaseMethodArgument setNodeType(String nodeType) {
        this.nodeType = nodeType;
        return this;
    }

    public Boolean getDoTree() {
        return doTree;
    }

    public BaseMethodArgument setDoTree(Boolean doTree) {
        this.doTree = doTree;
        return this;
    }

    public Boolean getDoCombine() {
        return doCombine;
    }

    public BaseMethodArgument setDoCombine(Boolean doCombine) {
        this.doCombine = doCombine;
        return this;
    }

    public Boolean getMoreRoot() {
        return moreRoot;
    }

    public BaseMethodArgument setMoreRoot(Boolean moreRoot) {
        this.moreRoot = moreRoot;
        return this;
    }

    public Boolean getColumnLazy() {
        return columnLazy;
    }

    public BaseMethodArgument setColumnLazy(Boolean columnLazy) {
        this.columnLazy = columnLazy;
        return this;
    }

    public Boolean getMark() {
        return mark;
    }

    public BaseMethodArgument setMark(Boolean mark) {
        this.mark = mark;
        return this;
    }

    public Boolean getPostil() {
        return postil;
    }

    public BaseMethodArgument setPostil(Boolean postil) {
        this.postil = postil;
        return this;
    }

    public Boolean getRemoveAll() {
        return removeAll;
    }

    public BaseMethodArgument setRemoveAll(Boolean removeAll) {
        this.removeAll = removeAll;
        return this;
    }

    public String getType() {
        return type;
    }

    public BaseMethodArgument setType(String type) {
        this.type = type;
        return this;
    }

    public String getValue() {
        return value;
    }

    public BaseMethodArgument setValue(String value) {
        this.value = value;
        return this;
    }

    public String getCode() {
        return code;
    }

    public BaseMethodArgument setCode(String code) {
        this.code = code;
        return this;
    }

    public String getJeFileSaveType() {
        return jeFileSaveType;
    }

    public BaseMethodArgument setJeFileSaveType(String jeFileSaveType) {
        this.jeFileSaveType = jeFileSaveType;
        return this;
    }

    public String getFileName() {
        return fileName;
    }

    public BaseMethodArgument setFileName(String fileName) {
        this.fileName = fileName;
        return this;
    }

    public String getRealPath() {
        return realPath;
    }

    public BaseMethodArgument setRealPath(String realPath) {
        this.realPath = realPath;
        return this;
    }

    public String getJeFileType() {
        return jeFileType;
    }

    public BaseMethodArgument setJeFileType(String jeFileType) {
        this.jeFileType = jeFileType;
        return this;
    }

    public String getContextType() {
        return contextType;
    }

    public BaseMethodArgument setContextType(String contextType) {
        this.contextType = contextType;
        return this;
    }

    public String getPath() {
        return path;
    }

    public BaseMethodArgument setPath(String path) {
        this.path = path;
        return this;
    }

    public Boolean getFuncEdit() {
        return funcEdit;
    }

    public BaseMethodArgument setFuncEdit(Boolean funcEdit) {
        this.funcEdit = funcEdit;
        return this;
    }

    public String getZhId() {
        return zhId;
    }

    public BaseMethodArgument setZhId(String zhId) {
        this.zhId = zhId;
        return this;
    }

    public String getZhMc() {
        return zhMc;
    }

    public BaseMethodArgument setZhMc(String zhMc) {
        this.zhMc = zhMc;
        return this;
    }

    public String getCascade() {
        return cascade;
    }

    public boolean hashCascade() {
        return "1".equals(cascade) || "true".equals(cascade);
    }

    public BaseMethodArgument setCascade(String cascade) {
        this.cascade = cascade;
        return this;
    }

    public String getjQuery() {
        return jQuery;
    }

    public BaseMethodArgument setjQuery(String jQuery) {
        this.jQuery = jQuery;
        return this;
    }

    public String getCustomQuery() {
        return customQuery;
    }

    public BaseMethodArgument setCustomQuery(String customQuery) {
        this.customQuery = customQuery;
        return this;
    }

    public String getOtherParams() {
        return otherParams;
    }

    public BaseMethodArgument setOtherParams(String otherParams) {
        this.otherParams = otherParams;
        return this;
    }

    public QueryInfo buildQueryInfo(){
        if(Strings.isNullOrEmpty(queryInfo)){
            return null;
        }
        return JSON.parseObject(queryInfo,QueryInfo.class);
    }

    /**
     * 构建查询对象
     * @return
     */
    public Query buildQuery(){
        return Query.build(jQuery);
    }

    public NativeQuery buildNativeQuery(){
        NativeQuery nativeQuery = new NativeQuery();
        if (StringUtils.isNotBlank(jQuery)) {

        }
        return nativeQuery;
    }

    /**
     * 构建
     * @return
     */
    public Query buildCustomQuery(){
        return Query.build(customQuery);
    }

    public static BaseMethodArgument build(){
        return new BaseMethodArgument();
    }

    public String getParameter(String name) {
        if(Strings.isNullOrEmpty(otherParams)){
            return null;
        }
        JSONObject paramsJsonObj = JSON.parseObject(otherParams);
        return paramsJsonObj.containsKey(name)?paramsJsonObj.getString(name):null;
    }

    public String getHttpParameter(String name) {
        return null;
    }

}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.spring;

import cn.hutool.core.io.FastByteArrayOutputStream;
import com.alibaba.fastjson2.JSON;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.util.ExceptionUtil;
import javax.servlet.http.HttpServletRequest;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class ErrorLogModel {

    private static final  String LINE_SPEATATOR = System.lineSeparator();
    private String errorCode;
    private String errorMess;
    private String url;
    private Object errorParam;
    private Object requestParam;
    private String stacktrace;
    private String createTime;

    private ErrorLogModel() {}

    /**
     * TODO 暂不明确
     * @param exception
     * @param request
     * @return
     */
    public static ErrorLogModel builder(PlatformException exception, HttpServletRequest request) {
        String reqUrl=request.getRequestURI();
        ErrorLogModel model = new ErrorLogModel();
        model.errorCode = exception.getErrorCode();
        model.errorMess = exception.getErrorMsg();
        model.url = reqUrl;
        model.errorParam = formatErrorParam(exception.getErrorParam());
        model.requestParam=formatRequestParam(ExceptionUtil.buildRequestParam(request));
        model.stacktrace = stackToStr(exception);

        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-mm-dd HH:mm:ss");
        model.createTime = df.format(LocalDateTime.now());
        return model;
    }
    private static String formatRequestParam(Map<String,Object> requestParam) {
        if (requestParam == null) {
            return "";
        }
        StringBuffer buf = new StringBuffer();
        if(requestParam!=null) {
            buf.append("请求参数：");
            for(String key:requestParam.keySet()){
                buf.append(LINE_SPEATATOR).append("     "+key+"：");
                buf.append(JSON.toJSON(requestParam.get(key)));
            }
        }
        return buf.toString();
    }
    private static String formatErrorParam(Object param) {
        if (param == null) {
            return "";
        }

        if (!param.getClass().isArray()) {
            return param.toString();
        }

        Object[] objs = (Object[]) param;
        if (objs.length == 0) {
            return "";
        }

        StringBuffer buf = new StringBuffer();
        buf.append("异常参数：");
        for (int i = 0; i < objs.length; i++) {
//            if (i  0) {
                buf.append(LINE_SPEATATOR).append("     ");
//            }
            buf.append("参数" + (i + 1) + ": ");
            buf.append(JSON.toJSON(objs[i]));
        }
        return buf.toString();
    }

    private static String stackToStr(Throwable throwable) {
        final FastByteArrayOutputStream out = new FastByteArrayOutputStream();
        throwable.printStackTrace(new PrintStream(out));
        return out.toString();
    }

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorMess() {
        return errorMess;
    }

    public String getUrl() {
        return url;
    }

    public Object getErrorParam() {
        return errorParam;
    }

    public String getStacktrace() {
        return stacktrace;
    }
    @Override
    public String toString() {
        String tmpl = LINE_SPEATATOR + "JEERROR-%s: %s"
                + LINE_SPEATATOR + "请求链接: %s"
                + LINE_SPEATATOR + "%s"
                + LINE_SPEATATOR + "%s"
                + LINE_SPEATATOR + "异常堆栈: %s"
                + LINE_SPEATATOR;

        return String.format(tmpl, this.errorCode, this.errorMess, this.url,this.requestParam, this.errorParam, this.stacktrace);
    }
}

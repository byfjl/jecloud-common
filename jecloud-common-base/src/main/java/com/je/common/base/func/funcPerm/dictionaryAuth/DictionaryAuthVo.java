/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.func.funcPerm.dictionaryAuth;

import java.io.Serializable;
import java.util.List;

public class DictionaryAuthVo implements Serializable {
    /**
     * 授权开关
     */
    private String authOnOff;

    /**
     * 部门
     */
    private List<DictCanCelAuthScopeVo> dictCanCelAuthDeptList;
    /**
     * 角色
     */
    private List<DictCanCelAuthScopeVo> dictCanCelAuthRoleList;
    /**
     * 机构
     */
    private List<DictCanCelAuthScopeVo> dictCanCelAuthOrgList;


    public String getAuthOnOff() {
        return authOnOff;
    }

    public void setAuthOnOff(String authOnOff) {
        this.authOnOff = authOnOff;
    }

    public List<DictCanCelAuthScopeVo> getDictCanCelAuthDeptList() {
        return dictCanCelAuthDeptList;
    }

    public void setDictCanCelAuthDeptList(List<DictCanCelAuthScopeVo> dictCanCelAuthDeptList) {
        this.dictCanCelAuthDeptList = dictCanCelAuthDeptList;
    }

    public List<DictCanCelAuthScopeVo> getDictCanCelAuthRoleList() {
        return dictCanCelAuthRoleList;
    }

    public void setDictCanCelAuthRoleList(List<DictCanCelAuthScopeVo> dictCanCelAuthRoleList) {
        this.dictCanCelAuthRoleList = dictCanCelAuthRoleList;
    }

    public List<DictCanCelAuthScopeVo> getDictCanCelAuthOrgList() {
        return dictCanCelAuthOrgList;
    }

    public void setDictCanCelAuthOrgList(List<DictCanCelAuthScopeVo> dictCanCelAuthOrgList) {
        this.dictCanCelAuthOrgList = dictCanCelAuthOrgList;
    }
}

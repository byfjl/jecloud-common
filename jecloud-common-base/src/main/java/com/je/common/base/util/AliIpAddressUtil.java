/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.je.common.base.service.rpc.SystemVariableRpcService;
import com.je.common.base.spring.SpringContextHolder;
import org.apache.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * 阿里云高德地图IP识别
 */
public class AliIpAddressUtil {

    public static String host;
    public static String path;
    public static String appkey;
    public static String appsecret;
    public static String appcode;

    private static Logger logger = LoggerFactory.getLogger(AliIpAddressUtil.class);
//    static {
//        try {
//            Properties sys = PropertiesLoaderUtils.loadAllProperties("sysconfig.properties");
//            host = sys.getProperty("amap.ip.host");
//            path = sys.getProperty("amap.ip.path");
//            appkey = sys.getProperty("amap.ip.appkey");
//            appsecret = sys.getProperty("amap.ip.appsecret");
//            appcode = sys.getProperty("amap.ip.appcode");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public static void init(){
        if(StringUtil.isEmpty(host)){
            SystemVariableRpcService systemVariableService = SpringContextHolder.getBean(SystemVariableRpcService.class);
            host = systemVariableService.requireSystemVariable("ALIYUN_IP_HOST");
            path = systemVariableService.requireSystemVariable("ALIYUN_IP_PATH");
            appkey = systemVariableService.requireSystemVariable("ALIYUN_IP_APPKEY");
            appsecret = systemVariableService.requireSystemVariable("ALIYUN_IP_APPSECRET");
            appcode = systemVariableService.requireSystemVariable("ALIYUN_IP_APPCODE");
        }
    }

    public static HttpResponse getIpAddress(String ip) {
        init();
        String method = "GET";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap();
        querys.put("ip", ip);

        HttpResponse response = null;
        try {
            response = HttpUtils.doGet(host, path, method, headers, querys);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}

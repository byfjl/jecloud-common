/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.service.rpc;

import java.util.Map;

/**
 * @program: jecloud-common
 * @author: LIULJ
 * @create: 2021-02-25 19:07
 * @description: 系统变量统一服务，包括前端变量，后端变量
 */
public interface SystemVariableRpcService {

    /**
     * 重载缓存
     */
    void reload();

    /**
     * 写入后端变量
     * @param key
     * @param value
     */
    void writeBackVariable(String key,String value);

    /**
     * 获取后台变量
     * @param key
     * @return
     */
    String requireBackVariable(String key);

    /**
     * 写入前端变量
     * @param key
     * @param value
     */
    void writeFrontVariable(String key,String value);

    /**
     * 获取前端变量
     * @param key
     * @return
     */
    String requireFrontVariable(String key);

    /**
     * 获取系统变量
     * @param key
     * @return
     */
    String requireSystemVariable(String key);

    /**
     * 获取系统变量
     * @return
     */
    Map<String, Object> systemVariables();


    Map<String, Object> formatCurrentUserAndCachedVariables();
}

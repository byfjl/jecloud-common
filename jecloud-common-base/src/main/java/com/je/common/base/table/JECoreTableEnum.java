/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table;

import com.je.common.base.mapper.query.ConditionEnum;

public enum JECoreTableEnum {

    /**
     * 核心表
     */
    JE_CORE_TABLES("JE_CORE_TABLES"),
    /**
     * 是核心表
     */
    JE_CORE("JE_CORE"),
    /**
     * 是RBAC表
     */
    JE_RBAC("JE_RBAC");


    /**
     * 条件类型
     */
    private String value;

    JECoreTableEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    /**
     * 获取表类型
     *
     * @param tableCode 表编码
     */
    public static JECoreTableEnum getTableType(String tableCode) {
        if (JE_CORE_TABLES.getValue().contains(tableCode)) {
            return JE_CORE;
        }
        return null;
    }


}

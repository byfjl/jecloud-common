/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.cache;

import com.je.common.base.redis.service.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * 抽象列表缓存实现
 *
 * @param <T>
 */
public abstract class AbstractListCache<T> extends AbstractRegistCache<T> implements BaseListCacheService<T> {

    @Autowired
    public RedisCache redisCache;

    @Override
    public boolean enableFristLevel() {
        return false;
    }

    @Override
    public T leftPopCacheValue() {
        return (T) redisCache.listLeftPop(getCacheKey());
    }

    @Override
    public T leftPopCacheValue(String tenantId) {
        return (T) redisCache.listLeftPop(getTenantCacheKey(tenantId));
    }

    @Override
    public T rightPopCacheValue() {
        return (T) redisCache.listRightPop(getCacheKey());
    }

    @Override
    public T rightPopCacheValue(String tenantId) {
        return (T) redisCache.listRightPop(getTenantCacheKey(tenantId));
    }

    @Override
    public void leftPut(T value) {
        redisCache.listLeftPush(getCacheKey(), value);
    }

    @Override
    public void leftPut(String tenantId, T value) {
        redisCache.listLeftPush(getTenantCacheKey(tenantId), value);
    }

    @Override
    public void leftPut(List<T> values) {
        redisCache.listLeftPushAll(getCacheKey(), values);
    }

    @Override
    public void leftPut(String tenantId, List<T> values) {
        redisCache.listLeftPushAll(getTenantCacheKey(tenantId), values);
    }

    @Override
    public void rightPut(T value) {
        redisCache.listRightPush(getCacheKey(), value);
    }

    @Override
    public void rightPut(String tenantId, T value) {
        redisCache.listRightPush(getTenantCacheKey(tenantId), value);
    }

    @Override
    public void rightPut(List<T> values) {
        redisCache.listRightPushAll(getCacheKey(), values);
    }

    @Override
    public void rightPut(String tenantId, List<T> values) {
        redisCache.listRightPushAll(getTenantCacheKey(tenantId), values);
    }

    @Override
    public List<T> getAllValues() {
        long size = redisCache.listSize(getCacheKey());
        return (List<T>) redisCache.listRange(getCacheKey(), 0L, size);
    }

    @Override
    public void clear() {
        redisCache.remove(getCacheKey());
    }

    @Override
    public void clear(String tenantId) {
        redisCache.remove(getTenantCacheKey(tenantId));
    }

    @Override
    public void localClear() {
        throw new CacheException(String.format("The Cache %s 1 level is not enabled!", getCacheKey()));
    }

    @Override
    public void localClear(String tenantId) {
        throw new CacheException(String.format("The Cache %s 1 level is not enabled!", getCacheKey()));
    }

}

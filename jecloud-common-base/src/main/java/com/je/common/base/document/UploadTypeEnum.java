/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.document;

/**
 * 附件上传类型枚举
 *
 * @author wangmm@ketr.com.cn
 * @date 2019/8/27
 */
public enum UploadTypeEnum {

    /**
     * 表单上传:单附件上传;多附件上传;
     */
    FORM("FORM"),

    /**
     * 功能附件上传;
     */
    FUNC("FUNC"),

    /**
     * 流程附件
     */
    FLOW("FLOW"),

    /**
     * 富文本编辑器上传文件
     */
    CKEDITOR("CKEDITOR"),

    /**
     * 附件子功能上传
     */
    SUBFUNC("SUBFUNC"),

    /**
     * IM附件;
     */
    IM("IM"),


    /**
     * 网盘附件
     */
    NETDISK("NETDISK"),

    /**
     * 临时
     */
    TEMP("TEMP"),

    /**
     * 其他
     */
    OTHER("OTHER");

    private String code;

    private UploadTypeEnum(String code) {
        this.code = code;
    }

    public static UploadTypeEnum getDefault(String code) {
        if (code == null || "".equals(code)) {
            return OTHER;
        }

        for (UploadTypeEnum value : UploadTypeEnum.values()) {
            if (value.getCode().equals(code)) {
                return value;
            }
        }
        return OTHER;
    }

    public String getCode() {
        return code;
    }


}

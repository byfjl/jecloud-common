/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.util;

import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class HttpIOTool {
	private static  String Cookie = null;
	public static void downloadFile(String remoteFilePath, String localFilePath)
    {
        URL urlfile = null;
        HttpURLConnection httpUrl = null;
        BufferedInputStream bis = null;
        BufferedOutputStream bos = null;
        File f = new File(localFilePath);
        if(!f.getParentFile().exists()) {
        	f.getParentFile().mkdirs();
        }
        try
        {
            urlfile = new URL(remoteFilePath);
            httpUrl = (HttpURLConnection)urlfile.openConnection();
            if(Cookie == null){
            	String sessionId = "";
                Map hfs=httpUrl.getHeaderFields();
                Set<String> keys=hfs.keySet();
                for(String str:keys){
                    List<String> vs=(List)hfs.get(str);
                	if("Set-Cookie".equals(str)){
	                    for(String v:vs){
	                    	v = v.split(";")[0];
	                    	sessionId+=v+";";
	                    }
                	}	                    
                }
                Cookie = sessionId;
            }else{
            	System.out.println(Cookie);
            	httpUrl.addRequestProperty("Cookie", Cookie);
            }
            httpUrl.connect();
            bis = new BufferedInputStream(httpUrl.getInputStream());
            bos = new BufferedOutputStream(new FileOutputStream(f));
            int len = 2048;
            byte[] b = new byte[len];
            while ((len = bis.read(b)) != -1)
            {
                bos.write(b, 0, len);
            }
            bos.flush();
            bis.close();
            httpUrl.disconnect();
        }catch (Exception e){
            throw new PlatformException("HTTP文件工具类下载文件异常", PlatformExceptionEnum.JE_CORE_UTIL_HTTPIO_DOWNLOAD_ERROR,new Object[]{remoteFilePath,localFilePath},e);
        }finally{
            if(bis!=null){
                try {
                    bis.close();
                } catch (IOException e) {
                    throw new PlatformException("HTTP文件工具类关闭文件流异常", PlatformExceptionEnum.JE_CORE_UTIL_HTTPIO_CLOSEIO_ERROR,new Object[]{remoteFilePath,localFilePath},e);
                }
            }
            if(bos!=null){
                try {
                    bos.close();
                } catch (IOException e) {
                    throw new PlatformException("HTTP文件工具类关闭文件流异常", PlatformExceptionEnum.JE_CORE_UTIL_HTTPIO_CLOSEIO_ERROR,new Object[]{remoteFilePath,localFilePath},e);
                }
            }
        }
    }		
}

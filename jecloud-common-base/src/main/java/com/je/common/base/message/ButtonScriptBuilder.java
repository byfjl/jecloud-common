/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.message;

public class ButtonScriptBuilder {

    /**
     * 打开列表
     */
    private static final StringBuffer OPEN_FUNC_GRID_SCRIPT_TEMPLATE = new StringBuffer();
    /**
     * 打开表单
     */
    private static final StringBuffer OPEN_FUNC_FORM_SCRIPT_TEMPLATE = new StringBuffer();

    static {
        OPEN_FUNC_GRID_SCRIPT_TEMPLATE.append("const {showFunc} = JE.useSystem();");
        OPEN_FUNC_GRID_SCRIPT_TEMPLATE.append("showFunc('%s',  {isFuncForm:false });");
        OPEN_FUNC_GRID_SCRIPT_TEMPLATE.append("EventOptions.closeNotice();");
        OPEN_FUNC_FORM_SCRIPT_TEMPLATE.append("const {showFunc} = JE.useSystem();");
        OPEN_FUNC_FORM_SCRIPT_TEMPLATE.append("showFunc('%s',  {isFuncForm:true,beanId:'%s' });");
        OPEN_FUNC_FORM_SCRIPT_TEMPLATE.append("EventOptions.closeNotice();");
    }

    /**
     * 构建打开功能列表脚本
     * @param funcCode
     * @return
     */
    public static String buildOpenFuncGridScript(String funcCode){
        String script = String.format(OPEN_FUNC_GRID_SCRIPT_TEMPLATE.toString(),funcCode);
        return script;
    }

    /**
     * 构建打开功能表单脚本
     * @param funcCode
     * @return
     */
    public static String buildOpenFuncFormScript(String funcCode,String beanId){
        String script = String.format(OPEN_FUNC_FORM_SCRIPT_TEMPLATE.toString(),funcCode,beanId);
        return script;
    }

}

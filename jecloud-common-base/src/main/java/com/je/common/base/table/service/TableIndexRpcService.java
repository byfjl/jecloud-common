/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table.service;

import com.je.common.base.DynaBean;

import java.util.List;
import java.util.Map;

public interface TableIndexRpcService {
    /**
     * 查询元数据表的索引信息
     *
     * @param columnCode 字段编码
     * @param tableCode  表编码
     * @return
     */
    DynaBean getTableIndexInfo(String columnCode, String tableCode);

    /**
     * 添加索引元数据
     *
     * @param columnCode 字段编码
     * @param tableCode  表编码
     */
    DynaBean insertTableIndex(String columnCode, String tableCode, String indexName);

    /**
     * 在数据库中获取index信息
     * @param tableCode
     * @param columnCode
     * @return
     */
    List<Map<String, Object>> getIndexInfosToDatabase(String tableCode, String columnCode);

}

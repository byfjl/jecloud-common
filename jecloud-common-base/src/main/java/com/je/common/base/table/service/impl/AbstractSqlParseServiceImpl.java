/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table.service.impl;

import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.*;
import com.je.common.base.service.MetaService;
import com.je.common.base.table.service.SqlParseService;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.SQLSyntaxErrorException;
import java.util.List;

public abstract class AbstractSqlParseServiceImpl implements SqlParseService {

    @Autowired
    private MetaService metaService;

    @Override
    public SQLSelectStatement selectParse(String selectSql) throws SQLSyntaxErrorException {
        String dbType = metaService.getDbType().getDb();
        return selectParse(selectSql,dbType);
    }

    @Override
    public SQLSelectStatement selectParse(String selectSql, String dbType) throws SQLSyntaxErrorException {
        SQLStatement statement = parse(selectSql,dbType);
        return (SQLSelectStatement) statement;
    }

    @Override
    public SQLAlterStatement alertParse(String selectSql) throws SQLSyntaxErrorException {
        String dbType = metaService.getDbType().getDb();
        return alertParse(selectSql,dbType);
    }

    @Override
    public SQLAlterStatement alertParse(String selectSql, String dbType) throws SQLSyntaxErrorException {
        SQLStatement statement = parse(selectSql,dbType);
        return (SQLAlterStatement) statement;
    }

    private SQLStatement parse(String selectSql,String dbType) throws SQLSyntaxErrorException {
        List<SQLStatement> list = SQLUtils.parseStatements(selectSql, dbType);
        if (list.size() > 1) {
            throw new SQLSyntaxErrorException("MultiQueries is not supported,use single query instead ");
        }
        return list.get(0);
    }

    @Override
    public SQLInsertStatement insertParse(String sql) throws SQLSyntaxErrorException {
        String dbType = metaService.getDbType().getDb();
        return insertParse(sql,dbType);
    }

    @Override
    public SQLInsertStatement insertParse(String sql, String dbType) throws SQLSyntaxErrorException {
        SQLStatement statement = parse(sql,dbType);
        return (SQLInsertStatement) statement;
    }

    @Override
    public SQLUpdateStatement updateParse(String sql) throws SQLSyntaxErrorException {
        String dbType = metaService.getDbType().getDb();
        return updateParse(sql,dbType);
    }

    @Override
    public SQLUpdateStatement updateParse(String sql, String dbType) throws SQLSyntaxErrorException {
        SQLStatement statement = parse(sql,dbType);
        return (SQLUpdateStatement) statement;
    }

    @Override
    public SQLCreateTableStatement createTableParse(String createTableDdl) throws SQLSyntaxErrorException {
        String dbType = metaService.getDbType().getDb();
        return createTableParse(createTableDdl,dbType);
    }

    @Override
    public SQLCreateTableStatement createTableParse(String createTableDdl, String dbType) throws SQLSyntaxErrorException {
        SQLStatement statement = parse(createTableDdl,dbType);
        return (SQLCreateTableStatement) statement;
    }

    @Override
    public SQLCreateViewStatement createViewParse(String createViewDdl) throws SQLSyntaxErrorException {
        String dbType = metaService.getDbType().getDb();
        return createViewParse(createViewDdl,dbType);
    }

    @Override
    public SQLCreateViewStatement createViewParse(String createViewDdl, String dbType) throws SQLSyntaxErrorException {
        SQLStatement statement = parse(createViewDdl,dbType);
        return (SQLCreateViewStatement) statement;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.cache;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.je.common.base.cache.model.CacheSync;
import com.je.common.base.redis.service.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: jecloud-common
 * @author: LIULJ
 * @create: 2021-03-13 12:15
 * @description:
 */
public abstract class AbstractHashCache<V> extends AbstractRegistCache<V> implements BaseHashCache<V> {

    @Autowired
    private RedisCache redisCache;

    /**
     * 本地缓存
     */
    private Map<String, Map<String, V>> localCache = Maps.newHashMap();

    /**
     * 获取本地缓存
     *
     * @param tenant 租户
     * @return
     */
    private Map<String, V> getLocalCache(String... tenant) {
        if (!enableFristLevel()) {
            throw new CacheException(String.format("The Cache %s 1 level is not enabled!", getCacheKey()));
        }
        String tenantKey = NO_TENANT_KEY;
        if (tenant != null && tenant.length == 1) {
            tenantKey = tenant[0];
        }
        if (tenant.length > 1) {
            throw new CacheException("Illegal tenant params,only can set one params！");
        }
        return localCache.get(tenantKey);
    }

    @Override
    public V getCacheValue(String subKey) {
        Map<String, V> tempCache = null;
        if (enableFristLevel()) {
            tempCache = getLocalCache();
            if (tempCache != null && tempCache.containsKey(subKey)) {
                return tempCache.get(subKey);
            }
        }

        //如果一级缓存没有或没有启用一级缓存，则从二级缓存开始
        Object result = redisCache.hashGet(getCacheKey(), subKey);
        if (result == null) {
            return null;
        }
        V v = (V) result;

        //如果启用一级缓存
        if (enableFristLevel()) {
            if (tempCache == null) {
                tempCache = Maps.newHashMap();
            }
            tempCache.put(subKey, v);
            localCache.put(NO_TENANT_KEY, tempCache);
        }

        return v;
    }

    @Override
    public V getCacheValue(String tenantId, String subKey) {
        Map<String, V> tempCache = null;
        if (enableFristLevel()) {
            tempCache = getLocalCache(tenantId);
            if (tempCache != null && tempCache.containsKey(subKey)) {
                return tempCache.get(subKey);
            }
        }

        //如果一级缓存没有或没有启用一级缓存，则从二级缓存开始
        Object result = redisCache.hashGet(getTenantCacheKey(tenantId), subKey);
        if (result == null) {
            return null;
        }
        V v = (V) result;

        //如果启用一级缓存，则缓存至一级缓存
        if (enableFristLevel()) {
            if (tempCache == null) {
                tempCache = Maps.newHashMap();
            }
            tempCache.put(subKey, v);
            localCache.put(NO_TENANT_KEY, tempCache);
        }

        return v;
    }

    @Override
    public Map<String, V> getCacheValues() {

        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache();
            if (tempCache != null) {
                return tempCache;
            }
        }

        //如果没有从一级缓存获取到，则从二级缓存获取
        Map<String, V> result = new HashMap<>();
        redisCache.hashGet(getCacheKey()).forEach((key, value) -> {
            result.put(key.toString(), (V) value);
        });

        if (enableFristLevel()) {
            localCache.put(NO_TENANT_KEY, result);
        }

        return result;
    }

    @Override
    public Map<String, V> getCacheValues(String tenantId) {

        //如果启用一级缓存
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache(tenantId);
            if (tempCache != null) {
                return tempCache;
            }
        }

        Map<String, V> result = new HashMap<>();
        redisCache.hashGet(getTenantCacheKey(tenantId)).forEach((key, value) -> {
            result.put(key.toString(), (V) value);
        });

        //如果启用一级缓存
        if (enableFristLevel()) {
            localCache.put(tenantId, result);
        }

        return result;
    }

    @Override
    public List<V> getCacheValues(List<String> subKeys) {
        List<V> result = new ArrayList<>();
        //如果启用一级缓存
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache();
            subKeys.forEach(subKey -> {
                if (tempCache.containsKey(subKey)) {
                    result.add(tempCache.get(subKey));
                }
            });
            return result;
        }

        redisCache.hashGet(getCacheKey(), subKeys).forEach(value -> {
            result.add((V) value);
        });

        return result;
    }

    @Override
    public List<V> getCacheValues(String tenantId, List<String> subKeys) {
        Map<String, V> tempCache = getLocalCache(tenantId);
        List<V> result = new ArrayList<>();

        if (enableFristLevel()) {
            subKeys.forEach(subKey -> {
                if (tempCache.containsKey(subKey)) {
                    result.add(tempCache.get(subKey));
                }
            });
        }

        redisCache.hashGet(getTenantCacheKey(tenantId), subKeys).forEach(value -> {
            result.add((V) value);
        });

        return result;
    }

    @Override
    public void putCache(String subKey, V value) {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache();
            if (tempCache == null) {
                tempCache = Maps.newHashMap();
                localCache.put(NO_TENANT_KEY, tempCache);
            }
            tempCache.put(subKey, value);
        }
        redisCache.hashPut(getCacheKey(), subKey, value);
    }

    @Override
    public void putCache(String tenantId, String subKey, V value) {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache(tenantId);
            if (tempCache == null) {
                tempCache = Maps.newHashMap();
                localCache.put(tenantId, tempCache);
            }
            tempCache.put(subKey, value);
        }
        redisCache.hashPut(getTenantCacheKey(tenantId), subKey, value);
    }

    @Override
    public void putMapCache(Map<String, V> map) {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache();
            if (tempCache == null) {
                tempCache = Maps.newHashMap();
                localCache.put(NO_TENANT_KEY, tempCache);
            }
            tempCache.putAll(map);
        }

        Map<String, Object> keyMaps = Maps.newHashMap();
        for (Map.Entry<String, V> eachEntry : map.entrySet()) {
            keyMaps.put(eachEntry.getKey(), eachEntry.getValue());
        }
        redisCache.hashPutAll(getCacheKey(), keyMaps);
    }

    @Override
    public void putMapCache(String tenantId, Map<String, V> map) {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache(tenantId);
            if (tempCache == null) {
                tempCache = Maps.newHashMap();
                localCache.put(NO_TENANT_KEY, tempCache);
            }
            tempCache.putAll(map);
        }

        Map<String, Object> keyMaps = Maps.newHashMap();
        for (Map.Entry<String, V> eachEntry : map.entrySet()) {
            keyMaps.put(eachEntry.getKey(), eachEntry.getValue());
        }
        redisCache.hashPutAll(getTenantCacheKey(tenantId), keyMaps);
    }

    @Override
    public void localClear() {
        if (!enableFristLevel()) {
            throw new CacheException(String.format("The Cache %s 1 level is not enabled!", getCacheKey()));
        }
        Map<String, V> tempCache = getLocalCache();
        if (tempCache != null) {
            tempCache.clear();
        }
    }

    @Override
    public void localRemove(List<String> key) {
        if (!enableFristLevel()) {
            throw new CacheException(String.format("The Cache %s 1 level is not enabled!", getCacheKey()));
        }
        Map<String, V> tempCache = getLocalCache();
        if (tempCache != null) {
            tempCache.remove(key);
        }
    }

    @Override
    public void localClear(String tenantId) {
        if (!enableFristLevel()) {
            throw new CacheException(String.format("The Cache %s 1 level is not enabled!", getCacheKey()));
        }
        Map<String, V> tempCache = getLocalCache(tenantId);
        if (tempCache != null) {
            tempCache.clear();
        }
    }

    @Override
    public void localRemove(String tenantId, List<String> key) {
        if (!enableFristLevel()) {
            throw new CacheException(String.format("The Cache %s 1 level is not enabled!", getCacheKey()));
        }
        Map<String, V> tempCache = getLocalCache(tenantId);
        if (tempCache != null) {
            tempCache.remove(key);
        }
    }

    @Override
    public void removeCache(String subKey) {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache();
            if (tempCache != null) {
                tempCache.remove(subKey);
            }
        }

        redisCache.hashDelete(getCacheKey(), subKey);
        if (enableFristLevel()) {
            CacheSync cacheSync = new CacheSync();
            cacheSync.setBeanName(getBeanName());
            cacheSync.setKey(Lists.newArrayList(subKey));
            sysnc(cacheSync);
        }
    }

    @Override
    public void removeCache(String tenantId, String subKey) {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache(tenantId);
            if (tempCache != null) {
                tempCache.remove(subKey);
            }
        }

        redisCache.hashDelete(getTenantCacheKey(tenantId), subKey);
        if (enableFristLevel()) {
            CacheSync cacheSync = new CacheSync();
            cacheSync.setBeanName(getBeanName());
            cacheSync.setKey(Lists.newArrayList(subKey));
            cacheSync.setTenantId(tenantId);
            sysnc(cacheSync);
        }

    }

    @Override
    public void removeCache(List<String> subKeys) {
        if (subKeys.size() == 0) {
            return;
        }
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache();
            if (tempCache != null) {
                tempCache.remove(subKeys);
            }
        }

        redisCache.hashDelete(getCacheKey(), subKeys.stream().toArray(String[]::new));

        if (enableFristLevel()) {
            CacheSync cacheSync = new CacheSync();
            cacheSync.setBeanName(getBeanName());
            cacheSync.setKey(subKeys);
            sysnc(cacheSync);
        }
    }

    @Override
    public void removeCache(String tenantId, List<String> subKeys) {
        if (subKeys.size() == 0) {
            return;
        }
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache(tenantId);
            if (tempCache != null) {
                tempCache.remove(subKeys);
            }
        }

        redisCache.hashDelete(getTenantCacheKey(tenantId), subKeys.stream().toArray(String[]::new));

        if (enableFristLevel()) {
            CacheSync cacheSync = new CacheSync();
            cacheSync.setBeanName(getBeanName());
            cacheSync.setKey(subKeys);
            cacheSync.setTenantId(tenantId);
            sysnc(cacheSync);
        }

    }

    @Override
    public void clear() {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache();
            if (tempCache != null) {
                tempCache.clear();
            }
        }

        redisCache.delByPrefix(getCacheKey(), "/");

        if (enableFristLevel()) {
            CacheSync cacheSync = new CacheSync();
            cacheSync.setBeanName(getBeanName());
            sysnc(cacheSync);
        }

    }

    @Override
    @Async
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void asyncClear() {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache();
            if (tempCache != null) {
                tempCache.clear();
            }
        }

        redisCache.delByPrefix(getCacheKey(), "/");

        if (enableFristLevel()) {
            CacheSync cacheSync = new CacheSync();
            cacheSync.setBeanName(getBeanName());
            sysnc(cacheSync);
        }

    }

    @Override
    public void clear(String tenantId) {
        if (enableFristLevel()) {
            Map<String, V> tempCache = getLocalCache(tenantId);
            if (tempCache != null) {
                tempCache.clear();
            }
        }

        redisCache.remove(getTenantCacheKey(tenantId));

        if (enableFristLevel()) {
            CacheSync cacheSync = new CacheSync();
            cacheSync.setBeanName(getBeanName());
            cacheSync.setTenantId(tenantId);
            sysnc(cacheSync);
        }

    }

    private void sysnc(CacheSync cacheSync) {
        String sysncKey = String.format("%s/%s", Cache.SYNC_KEY_PREFIX, System.currentTimeMillis());
        redisCache.put(sysncKey, cacheSync);
        redisCache.expire(sysncKey, Cache.SYNC_TIMEOUT);
    }

}

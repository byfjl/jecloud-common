/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.workflow;

public class WfAssgineUserType {
	/**
	 * 用户ID
	 */
	public final static String USERID ="USERID";
	/**
	 * 角色编码
	 */
	public final static String ROLEID ="ROLEID";
	/**
	 * 部门编码
	 */
	public final static String DEPTID ="DEPTID";
	/**
	 * 岗位编码
	 */
	public final static String SENTRYID ="SENTRYID";
	/**
	 * 创建人
	 */
	public final static String CREATE_USER ="CREATE_USER";
	/**
	 * 修改人
	 */
	public final static String MODIFY_USER ="MODIFY_USER";
	/**
	 * 用户ID数组
	 */
	public final static String USER_ARRAY ="USER_ARRAY";
}

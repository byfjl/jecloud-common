/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.constants.push;

import com.google.common.base.Strings;
import java.util.HashMap;
import java.util.Map;


public class PushType {

    private static Map<String, String> pushs = new HashMap<>();

    /***
     * 流程推送数字
     */
    public static final String WF = "WF";
    public static final String WF_NAME = "流程消息";
    /**
     * 消息(原小铃铛 JE_CORE_USERMSG)
     */
    public static final String MSG = "MSG";

    public static final String MSG_NAME = "系统信息";
    /**
     * 冒泡
     */
    public static final String BUBBLE = "BUBBLE";

    public static final String BUBBLE_NAME = "冒泡消息";
    /**
     * IM聊天
     */
    public static final String IM = "IM";
    public static final String IM_NAME = "IM消息";
    /**
     * 冒泡功能
     */
    public static final String BUBBLEFUNC = "BUBBLEFUNC";
    public static final String BUBBLEFUNC_NAME = "BUBBLEFUNC";
    /**
     * 批注
     */
    public static final String POSTIL = "POSTIL";
    public static final String POSTIL_NAME = "批注消息";
    /**
     * 事务交办
     */
    public static final String TRANSACTION = "TRANSACTION";
    public static final String TRANSACTION_NAME = "事务消息";
    /**
     * 日历任务
     */
    public static final String CALENDAR = "CALENDAR";
    public static final String CALENDAR_NAME = "日历消息";
    /**
     * 新闻数值
     */
    public static final String NOTICE = "NOTICE";
    public static final String NOTICE_NAME = "新闻消息";
    /**
     * 工作日志
     */
    public static final String RZ = "RZ";
    public static final String RZ_NAME = "日志消息";
    /**
     * 微邮
     */
    public static final String WY = "MICROMAIL";
    public static final String WY_NAME = "微邮消息";

    public static final String  DISK = "DISK";
    public static final String DISK_NAEME = "网盘消息";

    /**
     * APP功能
     */
    public static final String APPFUNC = "APPFUNC";
    public static final String APPFUNC_NAME = "APP消息";

    static {
        pushs.put(WF,WF_NAME);
        pushs.put(MSG,MSG_NAME);
        pushs.put(BUBBLE,BUBBLE_NAME);
        pushs.put(BUBBLEFUNC,BUBBLEFUNC_NAME);
        pushs.put(POSTIL,POSTIL_NAME);
        pushs.put(TRANSACTION,TRANSACTION_NAME);
        pushs.put(CALENDAR,CALENDAR_NAME);
        pushs.put(NOTICE,NOTICE_NAME);
        pushs.put(RZ,RZ_NAME);
        pushs.put(APPFUNC,APPFUNC_NAME);
        pushs.put(WY,WY_NAME);
        pushs.put(DISK,DISK_NAEME);
    }

    public static String getNameByCode(String code) {
        if (Strings.isNullOrEmpty(code)) {
            return "";
        }
        return pushs.get(APPFUNC)==null?"系统消息":pushs.get(APPFUNC);
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.cache;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * 注册cache
 * @param <T>
 */
public abstract class AbstractRegistCache<T> implements Cache<T>, InitializingBean {

    public static final String CACHA_REGIST_KEY = "jecloud/caches";

    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private Environment environment;

    @Override
    public void regist() {
        CacheRegistry cacheRegistry = new CacheRegistry(getName(),getCacheKey(),getService(),getDesc());
        redisTemplate.opsForHash().put(CACHA_REGIST_KEY,getCacheKey(),cacheRegistry);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        regist();
    }

    @Override
    public String getService() {
        return environment.getProperty("servicecomb.service.name");
    }



}

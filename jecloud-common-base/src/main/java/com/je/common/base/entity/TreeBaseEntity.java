/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.entity;

import com.google.common.base.Strings;
import com.je.common.base.db.FieldChineseName;
import com.je.common.base.entity.tree.TreeNode;
import com.je.common.base.constants.tree.TreeNodeType;
import com.je.common.base.util.EntityUtils;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public abstract class TreeBaseEntity extends BaseEntity {

	private static final long serialVersionUID = -3452336146907717019L;

	@FieldChineseName(value="层数")
	private Integer layer ;//所在的层数
	@TreeNode(type=TreeNodeType.NODETYPE)
	@FieldChineseName(value="结点类型")
	private String nodeType;//结点类型
	@FieldChineseName(value="父对象ID")
	private String parentId; // for ext-js show
	@TreeNode(type=TreeNodeType.NODEINFO)
	@FieldChineseName(value="功能信息")
	private String nodeInfo; // 功能信息，可能是URL，或一个相对路径
	@TreeNode(type=TreeNodeType.NODEINFOTYPE)
	@FieldChineseName(value="功能信息类型")
	private String nodeInfoType; //
	@TreeNode(type=TreeNodeType.NODEPATH)
	private String path;

	public Integer getLayer() {
		return layer;
	}

	public String getNodeType() {
		return nodeType;
	}

	public void setLayer(Integer layer) {
		this.layer = layer;
	}
	public void setNodeType(String nodeType) {
		this.nodeType = nodeType;
	}

	@Transient
	public abstract TreeBaseEntity getParent();

	@Transient
	public String getParentId() {
		TreeBaseEntity parent = this.getParent();
		if(null != parent) {
			String parentPkValue = EntityUtils.getInstance().getEntityIdValue(parent);
			if(!Strings.isNullOrEmpty(parentPkValue)) {
				parentId = parentPkValue;
			}
		}
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	@Column(length=2000)
	public String getNodeInfo() {
		return nodeInfo;
	}
	public void setNodeInfo(String nodeInfo) {
		this.nodeInfo = nodeInfo;
	}
	public String getNodeInfoType() {
		return nodeInfoType;
	}
	public void setNodeInfoType(String nodeInfoType) {
		this.nodeInfoType = nodeInfoType;
	}
	@Column(length=4000)
	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}


}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.func.funcPerm.roleSqlAuth;

import java.io.Serializable;
import java.util.List;

public class RoleSqlAuthVo implements Serializable {
    /**
     * 授权开关
     */
    private String authOnOff;


    /**
     * 部门授权
     */
    private List<RoleSqlAuthScopeVo> roleSqlAuthDeptList;
    /**
     * 角色授权
     */
    private List<RoleSqlAuthScopeVo> roleSqlAuthRoleList;
    /**
     * 机构授权
     */
    private List<RoleSqlAuthScopeVo> roleSqlAuthOrgList;

    public String getAuthOnOff() {
        return authOnOff;
    }

    public void setAuthOnOff(String authOnOff) {
        this.authOnOff = authOnOff;
    }


    public List<RoleSqlAuthScopeVo> getRoleSqlAuthDeptList() {
        return roleSqlAuthDeptList;
    }

    public void setRoleSqlAuthDeptList(List<RoleSqlAuthScopeVo> roleSqlAuthDeptList) {
        this.roleSqlAuthDeptList = roleSqlAuthDeptList;
    }

    public List<RoleSqlAuthScopeVo> getRoleSqlAuthRoleList() {
        return roleSqlAuthRoleList;
    }

    public void setRoleSqlAuthRoleList(List<RoleSqlAuthScopeVo> roleSqlAuthRoleList) {
        this.roleSqlAuthRoleList = roleSqlAuthRoleList;
    }

    public List<RoleSqlAuthScopeVo> getRoleSqlAuthOrgList() {
        return roleSqlAuthOrgList;
    }

    public void setRoleSqlAuthOrgList(List<RoleSqlAuthScopeVo> roleSqlAuthOrgList) {
        this.roleSqlAuthOrgList = roleSqlAuthOrgList;
    }
}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.base.table.service.impl;

import com.je.common.base.constants.ConstantVars;
import com.je.common.base.constants.table.ColumnType;
import com.je.common.base.datasource.runner.SqlRunner;
import com.je.common.base.db.DbFieldVo;
import com.je.common.base.db.JEDatabase;
import com.je.common.base.entity.extjs.DbIndex;
import com.je.common.base.entity.extjs.DbModel;
import com.je.common.base.entity.extjs.Model;
import com.je.common.base.exception.PlatformException;
import com.je.common.base.exception.PlatformExceptionEnum;
import com.je.common.base.service.rpc.BeanService;
import com.je.common.base.service.MetaService;
import com.je.common.base.table.service.PCDataService;
import com.je.common.base.util.*;
import org.apache.servicecomb.provider.pojo.RpcSchema;
import org.springframework.beans.factory.annotation.Autowired;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@RpcSchema(schemaId = "pCDataService")
public class PcDataServiceImpl implements PCDataService {

    @Autowired
    private MetaService metaService;
    @Autowired
    private BeanService beanService;

    /**
     * 加载存储过程字段模型
     *
     * @param dataSource    数据来源
     * @param procedureName 程序名称
     * @param fieldVos      TODO 暂不明确
     * @return
     */
    @Override
    public List<Model> loadProcedure(String dataSource, String procedureName, List<DbFieldVo> fieldVos) {
        List<Model> models = new ArrayList<Model>();
        Object[] params = new Object[fieldVos.size()];
        Connection con = null;
        CallableStatement proc = null;
        ResultSet rs = null;
        try {
            if (StringUtil.isEmpty(dataSource) || "local".equals(dataSource)) {
                con = metaService.getConnection();
                proc = con.prepareCall("{call " + procedureName + "(" + getCallParams(params) + ")}");
                DbProduceUtil.registerParams(proc, fieldVos);
                proc.execute();
                if (JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_ORACLE)
                        || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_SHENTONG)) {
                    Integer index = DbProduceUtil.loadOutCursorIndex(fieldVos);
                    if (index > 0) {
                        rs = (ResultSet) proc.getObject(index);
                    } else {
                        rs = proc.getResultSet();
                    }
                } else {
                    rs = proc.getResultSet();
                }
                ResultSetMetaData data = rs.getMetaData();
                int count = data.getColumnCount();
                for (int i = 1; i <= count; i++) {
                    // 获得指定列的列名
                    String columnName = data.getColumnName(i);
                    Integer type = data.getColumnType(i);
                    Model model = DBSqlUtils.getExtModel(columnName, type);
                    if (model != null) {
                        models.add(model);
                    }
                }
            } else {
                SqlRunner sqlRunner = SqlRunner.getInstance(dataSource);
                return sqlRunner.loadProcedure("{call " + procedureName + "(" + getCallParams(params) + ")}", fieldVos);
            }
        } catch (Exception e) {
            throw new PlatformException("根据数据源获取存储过程列信息异常!", PlatformExceptionEnum.JE_CORE_DB_COLUMN_PROCEDURE_ERROR, new Object[]{dataSource, procedureName, fieldVos}, e);
//			e.printStackTrace();
        } finally {
            JdbcUtil.close(rs, proc, con);
        }
        return models;
    }

    private void setExecuteParams(CallableStatement proc, Object[] params) throws SQLException {
        if (params != null) {
            for (int i = 1; i <= params.length; i++) {
                Object paramVal = params[i - 1];
                if (paramVal instanceof Integer) {
                    Integer val = (Integer) params[i - 1];
                    proc.setInt(i, val);
                } else if (paramVal instanceof Float) {
                    Float val = (Float) params[i - 1];
                    proc.setFloat(i, val);
                } else if (paramVal instanceof Double) {
                    Double val = (Double) params[i - 1];
                    proc.setDouble(i, val);
                } else {
                    proc.setObject(i, params[i - 1]);
                }
            }
        }
    }

    /**
     * 加载SQL字段模型
     *
     * @param dataSource 数据来源
     * @param sql        sql语句
     * @param fieldVos   TODO 暂不明确
     * @return
     */
    @Override
    public List<Model> loadSql(String dataSource, String sql, List<DbFieldVo> fieldVos) {
        List<Model> models = new ArrayList<Model>();
        Connection connection = null;
        CallableStatement proc = null;
        ResultSet resultSet = null;
        try {
            if (StringUtil.isEmpty(dataSource) || "local".equals(dataSource)) {
                connection = metaService.getConnection();
                proc = connection.prepareCall(sql);
                DbProduceUtil.registerParams(proc, fieldVos);
                proc.execute();
                resultSet = proc.getResultSet();
                ResultSetMetaData data = resultSet.getMetaData();
                int count = data.getColumnCount();
                for (int i = 1; i <= count; i++) {
                    // 获得指定列的列名
                    String columnName = data.getColumnName(i);
                    Integer type = data.getColumnType(i);
                    Model model = DBSqlUtils.getExtModel(columnName, type);
                    if (model != null) {
                        models.add(model);
                    }
                }
            } else {
                SqlRunner sqlRunner = SqlRunner.getInstance(dataSource);
                return sqlRunner.loadSql(sql, fieldVos);
            }
        } catch (Exception e) {
            throw new PlatformException("根据数据源获取SQL列信息异常!", PlatformExceptionEnum.JE_CORE_DB_COLUMN_SQL_ERROR, new Object[]{dataSource, sql, fieldVos}, e);
        } finally {
            JdbcUtil.close(resultSet, proc, connection);
        }
        return models;
    }

    /**
     * @param tableCode
     * @return
     */
    @Override
    public Boolean existsTable(String tableCode) {
        List<String> databaseTableNames = DataBaseUtils.getDataBaseTableNames();
        return databaseTableNames.contains(tableCode);
    }

    /**
     * 加载指定表名数据库字段模型
     *
     * @param tableCode 表code
     * @return
     */
    @Override
    public List<DbModel> loadTableColumn(String tableCode) {
        List<DbModel> models = new ArrayList<DbModel>();
        String sql = " SELECT * FROM " + tableCode + " WHERE 1!=1";
        List<DbFieldVo> fieldVos = new ArrayList<DbFieldVo>();
        Connection connection = null;
        CallableStatement proc = null;
        ResultSet resultSet = null;
        try {
            connection = metaService.getConnection();
            proc = connection.prepareCall(sql);
            DbProduceUtil.registerParams(proc, fieldVos);
            proc.execute();
            resultSet = proc.getResultSet();
            ResultSetMetaData data = resultSet.getMetaData();

            DatabaseMetaData databaseMetaData = connection.getMetaData();
            ResultSet resultSetPk = databaseMetaData.getPrimaryKeys(null,databaseMetaData.getUserName(),tableCode);
            String pkCode=null;
            if(resultSetPk.next()){
                 pkCode = resultSetPk.getString("COLUMN_NAME");
            }
            int count = data.getColumnCount();
            for (int i = 1; i <= count; i++) {
                // 获得指定列的列名,2022.05.07由data.getColumnName(i).toUpperCase()改为data.getColumnName(i)
                String columnName = data.getColumnName(i);
//					Integer type=data.getColumnType(i);
                String type = data.getColumnTypeName(i).toUpperCase();
                String typeLength = data.getPrecision(i) + "";
                int length = data.getPrecision(i);
                int scale = data.getScale(i);
                DbModel model = new DbModel();
                int nullAbleStatus = data.isNullable(i);
                if (nullAbleStatus == 0) {
                    model.setNull(false);
                } else {
                    model.setNull(true);
                }
                model.setCode(columnName);
//            	model.setLength(length);
                if (type.startsWith("VARCHAR")) {
                    model.setType(ColumnType.VARCHAR);
//						column.set("TABLECOLUMN_TYPE","VARCHAR");
                    if ("255".equals(length + "")) {
                        model.setType(ColumnType.VARCHAR255);
                        model.setLength("");
                    } else if ("100".equals(length + "")) {
                        model.setType(ColumnType.VARCHAR100);
                        model.setLength("");
                    } else if ("50".equals(length + "")) {
                        model.setType(ColumnType.VARCHAR50);
                        model.setLength("");
                    } else if ("30".equals(length + "")) {
                        model.setType(ColumnType.VARCHAR30);
                        model.setLength("");
                    } else if ("767".equals(length + "")) {
                        model.setType(ColumnType.VARCHAR767);
                        model.setLength("");
                    } else if ("1000".equals(length + "")) {
                        model.setType(ColumnType.VARCHAR1000);
                        model.setLength("");
                    } else if ("2000".equals(length + "")) {
                        model.setType(ColumnType.VARCHAR2000);
                        model.setLength("");
                    } else if ("4000".equals(length + "")) {
                        model.setType(ColumnType.VARCHAR4000);
                        model.setLength("");
                    } else if ("4".equals(length + "")) {
                        model.setType(ColumnType.YESORNO);
                        model.setLength("");
                    } else if (typeLength.length() > 4) {
                        model.setType(ColumnType.CLOB);
                        model.setLength("");
                    } else if (typeLength.length() > 0) {
                        model.setType(ColumnType.VARCHAR);
                        model.setLength(length + "");
                    }
                } else if (type.startsWith("INT")) {
                    model.setType(ColumnType.NUMBER);
                    model.setLength("");
                } else if (type.startsWith("FLOAT") || type.startsWith("NUMERIC") || type.startsWith("DECIMAL") || type.startsWith("NUMBER")) {
                    if (scale <= 0) {
                        model.setType(ColumnType.NUMBER);
                        if (length == 20) {
                            model.setLength("");
                        } else {
                            model.setLength(typeLength);
                        }
                    } else {
                        model.setType(ColumnType.FLOAT);
                        if (length == 20) {
                            typeLength = scale + "";
                        } else {
                            typeLength = length + "," + scale;
                        }
                        model.setLength(typeLength);
                    }
                } else if (type.startsWith("TEXT") || type.startsWith("LONGTEXT") || type.startsWith("CLOB")) {
                    model.setType(ColumnType.CLOB);
                    model.setLength("");

                } else {
                    model.setType(ColumnType.CUSTOM);
                    model.setLength(type);
                }
                /**
                 * 主键
                 */
                if(pkCode!=null&&pkCode.equals(columnName)){
                    model.setType(ColumnType.ID);
                }
                models.add(model);
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
//			e.printStackTrace();
            throw new PlatformException("根据表名获取列信息异常!", PlatformExceptionEnum.JE_CORE_DB_COLUMN_TABLE_ERROR, new Object[]{tableCode}, e);
        } finally {
            JdbcUtil.close(resultSet, proc, connection);
        }
        return models;
    }

    /**
     * 加载指定表名数据库字段模型
     *
     * @param tableCode 表code
     * @return
     */
    @Override
    public List<DbModel> loadTableColumnBySql(String tableCode) {
        List<DbModel> models = new ArrayList<DbModel>();
        if (ConstantVars.STR_ORACLE.equals(JEDatabase.getCurrentDatabase())
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_SHENTONG)
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_DM)
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_KINGBASEES)) {

            String selectSql = " select COLUMN_NAME,DATA_TYPE,DATA_LENGTH,DATA_PRECISION,DATA_SCALE,NULLABLE from user_tab_columns WHERE TABLE_NAME='" + tableCode + "'";

            // fix: 处理带用户名的 tableCode：如 tableCode = JEPAAS.JE_CORE_ENDUSER
            //是否是其他用户表
            if(tableCode.contains(".")) {
                int i = tableCode.lastIndexOf(".");
                //其他用户名
                String otherUser = tableCode.substring(0, i);
                //其他用户表名
                String userTableCode = tableCode.substring(i + 1, tableCode.length());
                selectSql = " select COLUMN_NAME,DATA_TYPE,DATA_LENGTH,DATA_PRECISION,DATA_SCALE,NULLABLE from all_tab_columns WHERE  owner = '" + otherUser + "' AND TABLE_NAME='" + userTableCode + "'";
            }

            List<Map<String,Object>> lists = metaService.selectSql(selectSql);
            for (Map columnInfo : lists) {
                String columnName = columnInfo.get("COLUMN_NAME") + "";
                String type = (columnInfo.get("DATA_TYPE") + "").toUpperCase();
                String typeLength = columnInfo.get("DATA_LENGTH") + "";
                String nullAble = columnInfo.get("NULLABLE") + "";
                int length = 0;
                int scale = 0;
                if (StringUtil.isNotEmpty(columnInfo.get("DATA_PRECISION") + "")) {
                    length = Integer.parseInt(columnInfo.get("DATA_PRECISION") + "");
                }
                if (StringUtil.isNotEmpty(columnInfo.get("DATA_SCALE") + "")) {
                    scale = Integer.parseInt(columnInfo.get("DATA_SCALE") + "");
                }
                models.add(buildDbModel(columnName, type, typeLength, length, scale, nullAble));
            }
        } else if (ConstantVars.STR_MYSQL.equals(JEDatabase.getCurrentDatabase())
                ||ConstantVars.STR_TIDB.equals(JEDatabase.getCurrentDatabase())) {
            List<Map<String,Object>> lists = metaService.selectSql(" show columns from " + tableCode + "");
            for (Map columnInfo : lists) {
                String columnName = columnInfo.get("Field") + "";
                String type = (columnInfo.get("Type") + "").toUpperCase();
                String typeLength = "";
                String nullAble = columnInfo.get("Null") + "";
                int length = 0;
                int scale = 0;
                if ("NO".equals(nullAble)) {
                    nullAble = "N";
                } else {
                    nullAble = "Y";
                }
                if (type.indexOf("(") != -1) {
                    typeLength = type.substring(type.indexOf("(") + 1, type.indexOf(")"));
                    if (typeLength.indexOf(",") != -1) {
                        length = Integer.parseInt(typeLength.split(",")[0]);
                        scale = Integer.parseInt(typeLength.split(",")[1]);
                        typeLength = length + "";
                    } else {
                        length = Integer.parseInt(typeLength);
                    }
                    type = type.substring(0, type.indexOf("("));
                }
                models.add(buildDbModel(columnName, type, typeLength, length, scale, nullAble));
            }
        } else {
            List<Map<String,Object>> lists = metaService.selectSql(" SELECT COLUMN_NAME,DATA_TYPE,CHARACTER_MAXIMUM_LENGTH,NUMERIC_PRECISION,NUMERIC_SCALE,IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME='" + tableCode + "'");
            for (Map columnInfo : lists) {
                String columnName = columnInfo.get("COLUMN_NAME") + "";
                String type = (columnInfo.get("DATA_TYPE") + "").toUpperCase();
                String typeLength = columnInfo.get("CHARACTER_MAXIMUM_LENGTH") + "";
                String nullAble = columnInfo.get("IS_NULLABLE") + "";
                int length = 0;
                int scale = 0;
                if ("NO".equals(nullAble)) {
                    nullAble = "N";
                } else {
                    nullAble = "Y";
                }
                if (StringUtil.isNotEmpty(typeLength)) {
                    length = Integer.parseInt(typeLength);
                }
                if (StringUtil.isNotEmpty(columnInfo.get("NUMERIC_PRECISION") + "")) {
                    length = Integer.parseInt(columnInfo.get("NUMERIC_PRECISION") + "");
                }
                if (StringUtil.isNotEmpty(columnInfo.get("NUMERIC_SCALE") + "")) {
                    scale = Integer.parseInt(columnInfo.get("NUMERIC_SCALE") + "");
                }
                models.add(buildDbModel(columnName, type, typeLength, length, scale, nullAble));
            }
        }
        return models;
    }

    private DbModel buildDbModel(String columnName, String type, String typeLength, int length, int scale, String nullAble) {
        DbModel model = new DbModel();
        model.setCode(columnName.toUpperCase());
        if ("N".equals(nullAble)) {
            model.setNull(false);
        } else {
            model.setNull(true);
        }
        if (type.startsWith("VARCHAR")) {
            model.setType(ColumnType.VARCHAR);
//				column.set("TABLECOLUMN_TYPE","VARCHAR");
            if ("255".equals(length + "")) {
                model.setType(ColumnType.VARCHAR255);
                model.setLength("");
            } else if ("100".equals(length + "")) {
                model.setType(ColumnType.VARCHAR100);
                model.setLength("");
            } else if ("50".equals(length + "")) {
                model.setType(ColumnType.VARCHAR50);
                model.setLength("");
            } else if ("30".equals(length + "")) {
                model.setType(ColumnType.VARCHAR30);
                model.setLength("");
            } else if ("767".equals(length + "")) {
                model.setType(ColumnType.VARCHAR767);
                model.setLength("");
            } else if ("1000".equals(length + "")) {
                model.setType(ColumnType.VARCHAR1000);
                model.setLength("");
            } else if ("2000".equals(length + "")) {
                model.setType(ColumnType.VARCHAR2000);
                model.setLength("");
            } else if ("4000".equals(length + "")) {
                model.setType(ColumnType.VARCHAR4000);
                model.setLength("");
            } else if ("4".equals(length + "")) {
                model.setType(ColumnType.YESORNO);
                model.setLength("");
            } else if (length > 4) {
                model.setType(ColumnType.CLOB);
                model.setLength(length + "");
            } else if (length > 0) {
                model.setType(ColumnType.VARCHAR);
                model.setLength(length + "");
            }
        } else if (type.startsWith("INT")) {
            model.setType(ColumnType.NUMBER);
            model.setLength("");
        } else if (type.startsWith("FLOAT") || type.startsWith("NUMERIC") || type.startsWith("DECIMAL") || type.startsWith("NUMBER")) {
            if (scale <= 0) {
                model.setType(ColumnType.NUMBER);
                if (length == 20) {
                    model.setLength("");
                } else {
                    model.setLength(typeLength);
                }
            } else {
                model.setType(ColumnType.FLOAT);
                if (length == 20) {
                    typeLength = scale + "";
                } else {
                    typeLength = length + "," + scale;
                }
                model.setLength(typeLength);
            }
        } else if (type.startsWith("TEXT") || type.startsWith("LONGTEXT") || type.startsWith("CLOB")) {
            model.setType(ColumnType.CLOB);
            model.setLength("");

        } else {
            model.setType(ColumnType.CUSTOM);
            model.setLength(type);
        }
        return model;
    }

    /**
     * 查询出表中的索引
     *
     * @param tableCode 表code
     * @return
     */
    @Override
    public List<DbIndex> loadTableIndex(String tableCode) {
        List<DbIndex> indexs = new ArrayList<DbIndex>();
        if (ConstantVars.STR_ORACLE.equals(JEDatabase.getCurrentDatabase())
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_SHENTONG)
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_KINGBASEES)
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_DM)) {
            List<Map<String,Object>> lists = metaService.selectSql(" select INDEX_NAME,TABLE_NAME,COLUMN_NAME from user_ind_columns WHERE TABLE_NAME='" + tableCode + "'");
            for (Map vals : lists) {
                DbIndex index = new DbIndex();
                index.setColumnName(vals.get("COLUMN_NAME") + "");
                index.setIndexName(vals.get("INDEX_NAME") + "");
                indexs.add(index);
            }
        } else if (ConstantVars.STR_MYSQL.equals(JEDatabase.getCurrentDatabase())
                ||ConstantVars.STR_TIDB.equals(JEDatabase.getCurrentDatabase())) {
            List<Map<String,Object>> lists = metaService.selectSql(" show index from " + tableCode + "");
            for (Map vals : lists) {
                DbIndex index = new DbIndex();
                index.setColumnName(vals.get("Column_name") + "");
                index.setIndexName(vals.get("Key_name") + "");
                indexs.add(index);
            }
        } else {
            StringBuffer querySql = new StringBuffer();
            querySql.append("SELECT a.NAME AS INDEX_NAME,c.NAME AS TABLE_NAME,d.NAME AS COLUMN_NAME ");
            querySql.append(" FROM   sysindexes   a  ");
            querySql.append(" JOIN   sysindexkeys   b   ON   a.id=b.id   AND   a.indid=b.indid  ");
            querySql.append(" JOIN   sysobjects   c   ON   b.id=c.id  ");
            querySql.append(" JOIN   syscolumns   d   ON   b.id=d.id   AND   b.colid=d.colid  ");
            querySql.append(" WHERE   a.indid   NOT IN(0,255)  ");
            querySql.append(" and   c.xtype='U'   ");
            querySql.append(" AND   c.name='" + tableCode + "'  ");
            List<Map<String,Object>> lists = metaService.selectSql(querySql.toString());
            for (Map vals : lists) {
                DbIndex index = new DbIndex();
                index.setColumnName(vals.get("COLUMN_NAME") + "");
                index.setIndexName(vals.get("INDEX_NAME") + "");
                indexs.add(index);
            }
        }
        return indexs;
    }

    /**
     * 加载数据库所有索引名称(不支持MySQL)
     *
     * @return
     */
    @Override
    public List<String> loadDbIndex() {
        // TODO Auto-generated method stub
        List<String> indexs = new ArrayList<String>();
        if (ConstantVars.STR_ORACLE.equals(JEDatabase.getCurrentDatabase())
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_SHENTONG)
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_KINGBASEES)
                || JEDatabase.getCurrentDatabase().equals(ConstantVars.STR_DM)) {
            List<Map<String,Object>> lists = metaService.selectSql(" select INDEX_NAME from user_ind_columns ");
            for (Map vals : lists) {
                indexs.add(vals.get("INDEX_NAME") + "");
            }
        } else if (ConstantVars.STR_MYSQL.equals(JEDatabase.getCurrentDatabase())
                ||ConstantVars.STR_TIDB.equals(JEDatabase.getCurrentDatabase())) {

        } else {
            StringBuffer querySql = new StringBuffer();
            querySql.append("SELECT a.NAME AS INDEX_NAME,c.NAME AS TABLE_NAME,d.NAME AS COLUMN_NAME ");
            querySql.append(" FROM   sysindexes   a  ");
            querySql.append(" JOIN   sysindexkeys   b   ON   a.id=b.id   AND   a.indid=b.indid  ");
            querySql.append(" JOIN   sysobjects   c   ON   b.id=c.id  ");
            querySql.append(" JOIN   syscolumns   d   ON   b.id=d.id   AND   b.colid=d.colid  ");
            querySql.append(" WHERE   a.indid   NOT IN(0,255)  ");
            querySql.append(" and   c.xtype='U'   ");
            List<Map<String,Object>> lists = metaService.selectSql(querySql.toString());
            for (Map vals : lists) {
                indexs.add(vals.get("INDEX_NAME") + "");
            }
        }
        return indexs;
    }

    /**
     * 构建存储过程占位参数
     *
     * @param params
     * @return
     */
    @Override
    public String getCallParams(Object[] params) {
        if (params == null) {
            return "";
        }
        String[] callParams = new String[params.length];
        for (int i = 0; i < callParams.length; i++) {
            callParams[i] = "?";
        }
        return StringUtil.buildSplitString(callParams, ",");
    }

}

/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.servicecomb;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.HexUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import java.io.File;

@Service
public class CheckStyleService implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(HexUtil.decodeHexStr("6a65636c6f7564"));

    private static final String FILE_PAHT = HexUtil.decodeHexStr("6c6963656e7365");

    private static final String FILE_NAME = HexUtil.decodeHexStr("6a65636c6f75642e6c6963656e7365");

    @Override
    public void afterPropertiesSet() throws Exception {
        String file = System.getProperty("user.home") + File.separator + FILE_PAHT + File.separator + FILE_NAME;
        if (!FileUtil.exist(file)) {
            logger.error(HexUtil.decodeHexStr("546865206c6963656e73652063616e27742062652066696e6465642120706c656173652073657474696e6720746865206a65636c6f75642e6c6963656e736520696e746f20746865206469726563746f727921"));
            System.exit(1);
        }
    }

}

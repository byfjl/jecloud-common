/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.servicecomb.filter;

import com.google.common.base.Strings;
import com.je.common.auth.token.AuthSession;
import com.je.common.auth.impl.account.Account;
import com.je.common.base.spring.SpringContextHolder;
import com.je.common.base.util.SecurityUserHolder;
import com.je.ibatis.extension.plugins.inner.tenant.DynaTenant;
import com.je.ibatis.extension.plugins.inner.tenant.DynaTenantContext;
import org.apache.servicecomb.core.Invocation;
import org.apache.servicecomb.foundation.common.http.HttpStatus;
import org.apache.servicecomb.foundation.vertx.http.HttpServletRequestEx;
import org.apache.servicecomb.swagger.invocation.Response;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;

public class SecurityUserServerFilter extends AbstractHttpServerFilter {

    /**
     * 认证token的Key
     */
    public static final String X_AUTH_TOKEN = "authorization";

    @Override
    public int getOrder() {
        return 100;
    }

    @Override
    public Response afterReceiveRequest(Invocation invocation, HttpServletRequestEx requestEx) {
        String token = invocation.getContext(X_AUTH_TOKEN);
        if(Strings.isNullOrEmpty(token)){
            return null;
        }
        RedisTemplate<String, Object> redisTemplate = SpringContextHolder.getBean("redisTemplate");
        StringRedisTemplate stringRedisTemplate = SpringContextHolder.getBean("stringRedisTemplate");
        String authId = stringRedisTemplate.opsForValue().get("authorization:token:" + token);
        if (Strings.isNullOrEmpty(authId)) {
            return buildRejectResponse("此token已失效，请重新登录！");
        }
        Object sessionObj = redisTemplate.opsForValue().get("authorization:session:" + authId);
        if (sessionObj == null) {
            return buildRejectResponse("此token已失效，请重新登录！");
        }
        AuthSession session = (AuthSession) sessionObj;
        if (!session.getDataMap().containsKey("account")) {
            return buildRejectResponse("无法根据token获取登录信息，请重新登录！");
        }
        Account account = (Account) session.getDataMap().get("account");
        if(!Strings.isNullOrEmpty(account.getTenantId())){
            DynaTenant tenant = new DynaTenant();
            tenant.setId(account.getTenantId());
            tenant.setName(account.getTenantName());
            DynaTenantContext.putTenant(tenant);
        }
        SecurityUserHolder.put(account);
        return null;
    }

    private Response buildRejectResponse(String message){
        return Response.create(new HttpStatus(401,"Unauthorized"),message);
    }

}

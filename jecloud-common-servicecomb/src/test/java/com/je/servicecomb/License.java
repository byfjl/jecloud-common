/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.servicecomb;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.HexUtil;
import com.alibaba.fastjson2.JSONObject;
import com.je.common.base.util.RandomUtil;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Random;

public class License {

    private static final String FILE_PAHT = "license";

    private static final String FILE_NAME = "jecloud.license";

    private static final String LICENSE_KEY = "LICENSE";

    private static final String COMPANY_KEY = "COMPANY";

    private static final String URL_KEY = "COMPANY_URL";

    public static void main(String[] args) {
        String folder = System.getProperty("user.home") + File.separator + FILE_PAHT;
        if (!FileUtil.exist(folder)) {
            FileUtil.mkdir(folder);
        }
        String license = generateSerialNumber();
        String company = "北京凯特伟业提供技术支持";
        String url = "https://jecloud.net";
        JSONObject licenseObj = new JSONObject();
        licenseObj.put(LICENSE_KEY,license);
        licenseObj.put(COMPANY_KEY,company);
        licenseObj.put(URL_KEY,url);
        String hexStr = HexUtil.encodeHexStr(licenseObj.toJSONString());
        String base64 = Base64.encode(hexStr.getBytes(StandardCharsets.UTF_8));
        FileUtil.writeString(base64,new File(folder + File.separator + FILE_NAME),"UTF-8");
        System.out.println(HexUtil.encodeHexStr("jecloud"));
        System.out.println(HexUtil.encodeHexStr("license"));
        System.out.println(HexUtil.encodeHexStr("jecloud.license"));
        System.out.println(HexUtil.encodeHexStr("The license can't be finded! please setting the jecloud.license into the directory!"));

    }

    public static String generateSerialNumber() {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 16; i++) {
            if (i > 0 && i % 4 == 0) {
                sb.append("-");
            }
            int digit = random.nextInt(10);
            sb.append(digit);
        }
        return sb.toString();
    }

}

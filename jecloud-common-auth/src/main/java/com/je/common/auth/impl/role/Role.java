/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.impl.role;

import com.je.common.auth.AuthRole;
import com.je.common.auth.impl.TenantModel;
import java.util.Map;
import static com.je.common.auth.util.BeanMapUtil.*;

/**
 * 系统角色
 */
public class Role extends TenantModel implements AuthRole {

    private static final long serialVersionUID = -1140520647085179348L;
    /**
     * 主键ID
     */
    private String id;
    /**
     * 编码
     */
    private String code;
    /**
     * 名称
     */
    private String name;
    /**
     * 基础角色
     */
    private Role baseRole;
    /**
     * 父级角色
     */
    private Role parent;
    /**
     * 角色路径
     */
    private String path;
    /**
     * 节点类型
     */
    private String nodeType;
    /**
     * 层级
     */
    private int layer;
    /**
     * 是否启用
     */
    private boolean disabled;
    /**
     * 树形排序
     */
    private String treeOrderIndex;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Role getBaseRole() {
        return baseRole;
    }

    public void setBaseRole(Role baseRole) {
        this.baseRole = baseRole;
    }

    public Role getParent() {
        return parent;
    }

    public void setParent(Role parent) {
        this.parent = parent;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getNodeType() {
        return nodeType;
    }

    public void setNodeType(String nodeType) {
        this.nodeType = nodeType;
    }

    public int getLayer() {
        return layer;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public String getTreeOrderIndex() {
        return treeOrderIndex;
    }

    public void setTreeOrderIndex(String treeOrderIndex) {
        this.treeOrderIndex = treeOrderIndex;
    }

    @Override
    public void parse(Map<String,Object> beanMap) {
        super.parse(beanMap);
        this.setId(getStr(beanMap,"JE_RBAC_ROLE_ID"));
        this.setCode(getStr(beanMap,"ROLE_CODE"));
        this.setName(getStr(beanMap,"ROLE_NAME"));
        this.setPath(getStr(beanMap,"SY_PATH"));
        this.setNodeType(getStr(beanMap,"SY_NODETYPE"));
        this.setLayer(getInteger(beanMap,"SY_LAYER"));
        this.setDisabled("1".equals(getStr(beanMap,"SY_DISABLED")) ? true : false);
        this.setTreeOrderIndex(getStr(beanMap,"SY_TREEORDERINDEX"));
    }

}

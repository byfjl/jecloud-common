/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.token;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Session Model
 *
 * @author kong
 */
public class AuthSession implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 在 Session 上存储角色时建议使用的key
     */
    public static final String ROLE_LIST = "ROLE_LIST";

    /**
     * 在 Session 上存储权限时建议使用的key
     */
    public static final String PERMISSION_LIST = "PERMISSION_LIST";

    /**
     * 在session上存储部门时建议使用的key
     */
    public static final String DEPT = "DEPT";

    /**
     * 在session上存储机构时建议使用的key
     */
    public static final String ORG = "ORG";

    /**
     * 此Session的id
     */
    private String id;

    /**
     * 此Session的创建时间
     */
    private long createTime;

    /**
     * 此Session的所有挂载数据
     */
    private final Map<String, Object> dataMap = new ConcurrentHashMap<>();

    /**
     * 此Session绑定的token签名列表
     */
    private final List<TokenSign> tokenSignList = new ArrayList<>();

    // ----------------------- 构建相关

    /**
     * 构建一个Session对象
     */
    public AuthSession() {
        /*
         * 当Session从Redis中反序列化取出时，框架会误以为创建了新的Session，
         * 因此此处不可以调用this(null); 避免监听器收到错误的通知
         */
        // this(null);
    }

    /**
     * 构建一个Session对象
     *
     * @param id Session的id
     */
    public AuthSession(String id) {
        this.id = id;
        this.createTime = System.currentTimeMillis();
    }

    /**
     * 获取此Session的id
     *
     * @return 此会话的id
     */
    public String getId() {
        return id;
    }

    /**
     * 写入此Session的id
     *
     * @param id SessionId
     * @return 对象自身
     */
    public AuthSession setId(String id) {
        this.id = id;
        return this;
    }

    public Map<String, Object> getDataMap() {
        return dataMap;
    }

    /**
     * 返回当前会话创建时间
     *
     * @return 时间戳
     */
    public long getCreateTime() {
        return createTime;
    }

    /**
     * 写入此Session的创建时间
     *
     * @param createTime 时间戳
     * @return 对象自身
     */
    public AuthSession setCreateTime(long createTime) {
        this.createTime = createTime;
        return this;
    }

    /**
     * 返回token签名列表的拷贝副本
     *
     * @return token签名列表
     */
    public List<TokenSign> getTokenSignList() {
        return tokenSignList;
    }

    /**
     * 添加签名
     *
     * @param tokenSign
     */
    public void addTokenSign(TokenSign tokenSign) {
        this.tokenSignList.add(tokenSign);
    }

    /**
     * 移除签名
     *
     * @param tokenSign
     */
    public boolean removeTokenSign(TokenSign tokenSign) {
        return this.tokenSignList.remove(tokenSign);
    }

    /**
     * 查找一个token签名
     *
     * @param tokenValue token值
     * @return 查找到的tokenSign
     */
    public TokenSign getTokenSign(String tokenValue) {
        for (TokenSign tokenSign : getTokenSignList()) {
            if (tokenSign.getValue().equals(tokenValue)) {
                return tokenSign;
            }
        }
        return null;
    }

}

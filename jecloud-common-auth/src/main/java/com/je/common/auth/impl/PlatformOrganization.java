/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.impl;

import com.je.common.auth.AuthOrg;
import java.util.Map;
import static com.je.common.auth.util.BeanMapUtil.*;

/**
 * 机构管理统一封装类
 */
public class PlatformOrganization extends TenantModel implements AuthOrg {

    private static final long serialVersionUID = -6518104925437653175L;

    /**
     * 主键ID
     */
    private String id;
    /**
     * 机构编码
     */
    private String code;
    /**
     * 机构名称
     */
    private String name;
    /**
     * 资源表ID
     */
    private String resourceTableId;
    /**
     * 资源表编码
     */
    private String resourceTableCode;
    /**
     * 资源表名称
     */
    private String resourceTableName;
    /**
     * 账户名称字段
     */
    private String accountNameField;
    /**
     * 账户编码字段
     */
    private String accountCodeField;
    /**
     * 账户主键字段
     */
    private String accountPkField;
    /**
     * 手机字段
     */
    private String phoneField;
    /**
     * 邮件字段
     */
    private String emailField;
    /**
     * 头像字段
     */
    private String avatarField;
    /**
     * 角色ID字段
     */
    private String roleIdField;
    /**
     * 角色名称字段
     */
    private String roleNameField;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getResourceTableId() {
        return resourceTableId;
    }

    public void setResourceTableId(String resourceTableId) {
        this.resourceTableId = resourceTableId;
    }

    public String getResourceTableCode() {
        return resourceTableCode;
    }

    public void setResourceTableCode(String resourceTableCode) {
        this.resourceTableCode = resourceTableCode;
    }

    public String getResourceTableName() {
        return resourceTableName;
    }

    public void setResourceTableName(String resourceTableName) {
        this.resourceTableName = resourceTableName;
    }

    public String getAccountNameField() {
        return accountNameField;
    }

    public void setAccountNameField(String accountNameField) {
        this.accountNameField = accountNameField;
    }

    public String getAccountCodeField() {
        return accountCodeField;
    }

    public void setAccountCodeField(String accountCodeField) {
        this.accountCodeField = accountCodeField;
    }

    public String getAccountPkField() {
        return accountPkField;
    }

    public void setAccountPkField(String accountPkField) {
        this.accountPkField = accountPkField;
    }

    public String getPhoneField() {
        return phoneField;
    }

    public void setPhoneField(String phoneField) {
        this.phoneField = phoneField;
    }

    public String getEmailField() {
        return emailField;
    }

    public void setEmailField(String emailField) {
        this.emailField = emailField;
    }

    public String getAvatarField() {
        return avatarField;
    }

    public void setAvatarField(String avatarField) {
        this.avatarField = avatarField;
    }

    public String getRoleIdField() {
        return roleIdField;
    }

    public void setRoleIdField(String roleIdField) {
        this.roleIdField = roleIdField;
    }

    public String getRoleNameField() {
        return roleNameField;
    }

    public void setRoleNameField(String roleNameField) {
        this.roleNameField = roleNameField;
    }

    @Override
    public void parse(Map<String,Object> beanMap) {
        super.parse(beanMap);
        setId(getStr(beanMap,"JE_RBAC_ORG_ID"));
        setCode(getStr(beanMap,"ORG_CODE"));
        setName(getStr(beanMap,"ORG_NAME"));
        setResourceTableId(getStr(beanMap,"JE_CORE_RESOURCETABLE_ID"));
        setResourceTableCode(getStr(beanMap,"ORG_RESOURCETABLE_CODE"));
        setResourceTableName(getStr(beanMap,"ORG_RESOURCETABLE_NAME"));
        setAccountCodeField(getStr(beanMap,"ORG_ACCOUNT_CODE"));
        setAccountNameField(getStr(beanMap,"ORG_ACCOUNT_NAME"));
        setAccountPkField(getStr(beanMap,"ORG_FIELD_PK"));
        setPhoneField(getStr(beanMap,"ORG_ACCOUNT_PHONE"));
        setEmailField(getStr(beanMap,"ORG_ACCOUNT_MAIL"));
        setAvatarField(getStr(beanMap,"ORG_ACCOUNT_AVATAR"));
        setRoleIdField(getStr(beanMap,"ORG_ROLEFIELD_ID"));
        setRoleNameField(getStr(beanMap,"ORG_ROLEFIELD_NAME"));
    }

}

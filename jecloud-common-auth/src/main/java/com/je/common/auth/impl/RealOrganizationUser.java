/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.impl;

import com.je.common.auth.AuthRealOrg;
import com.je.common.auth.AuthRealUser;

import java.util.Date;

/**
 * 平台统一机构用户真实实现，例如部门人员，供应商人员等
 */
public class RealOrganizationUser extends TenantModel implements AuthRealUser {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private String id;
    /**
     * 机构用户名称
     */
    private String name;
    /**
     * 机构用户编码
     */
    private String code;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 手机号
     */
    private String phone;

    /**
     * 生日
     */
    private Date birthDate;
    /**
     * 邮件
     */
    private String email;
    /**
     * 用户唯一Id
     */
    private String deptmentUserId;
    /**
     * 所属机构
     */
    private RealOrganization organization;

    public RealOrganizationUser() {
    }

    public RealOrganizationUser(RealOrganization organization) {
        this.organization = organization;
    }

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public RealOrganization getOrganization() {
        return organization;
    }

    public void setOrganization(RealOrganization organization) {
        this.organization = organization;
    }

    @Override
    public AuthRealOrg getRealOrg() {
        return organization;
    }

    public String getDeptmentUserId() {
        return deptmentUserId;
    }

    public void setDeptmentUserId(String deptmentUserId) {
        this.deptmentUserId = deptmentUserId;
    }
    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }
}

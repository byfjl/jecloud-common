/*
 * MIT License
 *
 * Copyright (c) 2023 北京凯特伟业科技有限公司
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.je.common.auth.impl;

import cn.hutool.core.date.DateUtil;
import com.google.common.base.Strings;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.je.common.auth.util.BeanMapUtil.getInteger;
import static com.je.common.auth.util.BeanMapUtil.getStr;

/**
 * 部门用户，构建来源，视图
 */
public class DepartmentUser extends RealOrganizationUser implements Serializable {

    private static final long serialVersionUID = 6781664570186884697L;
    /**
     * 照片信息
     */
    private String photo;
    /**
     * 直接领导
     */
    private String directLeaderId;
    /**
     * 直接领导
     */
    private String directLeaderCode;
    /**
     * 直接领导
     */
    private String directLeaderName;
    /**
     * 主管ID
     */
    private String majorId;
    /**
     * 主管编码
     */
    private String majorCode;
    /**
     * 主管名称
     */
    private String majorName;
    /**
     * 性别编码
     */
    private String sexCode;
    /**
     * 性别名称
     */
    private String sexName;
    /**
     * 是否是主管
     */
    private boolean major;
    /**
     * 是否已婚
     */
    private boolean married;
    /**
     * 证件编码
     */
    private String cardNum;
    /**
     * 证件类型编码
     */
    private String idTypeCode;
    /**
     * 证件类型名称
     */
    private String idTypeName;
    /**
     * 出生日期
     */
    private Date birthDate;
    /**
     * 年龄
     */
    private int age;
    /**
     * 入职日期
     */
    private Date entryDate;
    /**
     * 离职日期
     */
    private Date leaveDate;
    /**
     * 行政职务编码
     */
    private String postCode;
    /**
     * 行政职务名称
     */
    private String postName;
    /**
     * 文化程度编码
     */
    private String educationCode;
    /**
     * 文化程度名称
     */
    private String educationName;
    /**
     * 籍贯
     */
    private String nativePlace;
    /**
     * 是否初始化
     */
    private boolean init;
    /**
     * 监管单位
     */
    private List<String> monitorDepts;
    /**
     * 监管公司
     */
    private List<String> monitorCompanys;
    /**
     * 所属公司ID
     */
    private String companyId;
    /**
     * 所属公司名称
     */
    private String companyName;
    /**
     * 所属公司Code
     */
    private String companyCode;
    /**
     * 所属集团公司ID
     */
    private String groupCompanyId;
    /**
     * 所属集团公司名称
     */
    private String groupCompanyName;
    /**
     * 所属集团公司Code
     */
    private String groupCompanyCode;

    public DepartmentUser() {
    }

    public DepartmentUser(Department department) {
        super(department);
    }

    public String getDirectLeaderId() {
        return directLeaderId;
    }

    public void setDirectLeaderId(String directLeaderId) {
        this.directLeaderId = directLeaderId;
    }

    public String getDirectLeaderCode() {
        return directLeaderCode;
    }

    public void setDirectLeaderCode(String directLeaderCode) {
        this.directLeaderCode = directLeaderCode;
    }

    public String getDirectLeaderName() {
        return directLeaderName;
    }

    public void setDirectLeaderName(String directLeaderName) {
        this.directLeaderName = directLeaderName;
    }

    public String getMajorId() {
        return majorId;
    }

    public void setMajorId(String majorId) {
        this.majorId = majorId;
    }

    public String getMajorCode() {
        return majorCode;
    }

    public void setMajorCode(String majorCode) {
        this.majorCode = majorCode;
    }

    public String getMajorName() {
        return majorName;
    }

    public void setMajorName(String majorName) {
        this.majorName = majorName;
    }

    public String getSexCode() {
        return sexCode;
    }

    public void setSexCode(String sexCode) {
        this.sexCode = sexCode;
    }

    public String getSexName() {
        return sexName;
    }

    public void setSexName(String sexName) {
        this.sexName = sexName;
    }

    public boolean isMajor() {
        return major;
    }

    public void setMajor(boolean major) {
        this.major = major;
    }

    public boolean isMarried() {
        return married;
    }

    public void setMarried(boolean married) {
        this.married = married;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getCardNum() {
        return cardNum;
    }

    public void setCardNum(String cardNum) {
        this.cardNum = cardNum;
    }

    public String getIdTypeCode() {
        return idTypeCode;
    }

    public void setIdTypeCode(String idTypeCode) {
        this.idTypeCode = idTypeCode;
    }

    public String getIdTypeName() {
        return idTypeName;
    }

    public void setIdTypeName(String idTypeName) {
        this.idTypeName = idTypeName;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getPostName() {
        return postName;
    }

    public void setPostName(String postName) {
        this.postName = postName;
    }

    public String getEducationCode() {
        return educationCode;
    }

    public void setEducationCode(String educationCode) {
        this.educationCode = educationCode;
    }

    public String getEducationName() {
        return educationName;
    }

    public void setEducationName(String educationName) {
        this.educationName = educationName;
    }

    public String getNativePlace() {
        return nativePlace;
    }

    public void setNativePlace(String nativePlace) {
        this.nativePlace = nativePlace;
    }

    @Override
    public Date getBirthDate() {
        return birthDate;
    }

    @Override
    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    public Date getLeaveDate() {
        return leaveDate;
    }

    public void setLeaveDate(Date leaveDate) {
        this.leaveDate = leaveDate;
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }

    public List<String> getMonitorDepts() {
        return monitorDepts;
    }

    public void setMonitorDepts(List<String> monitorDepts) {
        this.monitorDepts = monitorDepts;
    }

    public List<String> getMonitorCompanys() {
        return monitorCompanys;
    }

    public void setMonitorCompanys(List<String> monitorCompanys) {
        this.monitorCompanys = monitorCompanys;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getGroupCompanyId() {
        return groupCompanyId;
    }

    public void setGroupCompanyId(String groupCompanyId) {
        this.groupCompanyId = groupCompanyId;
    }

    public String getGroupCompanyName() {
        return groupCompanyName;
    }

    public void setGroupCompanyName(String groupCompanyName) {
        this.groupCompanyName = groupCompanyName;
    }

    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }

    public String getGroupCompanyCode() {
        return groupCompanyCode;
    }

    public void setGroupCompanyCode(String groupCompanyCode) {
        this.groupCompanyCode = groupCompanyCode;
    }

    @Override
    public void parse(Map<String, Object> beanMap) {
        super.parse(beanMap);
        this.setId(getStr(beanMap, "JE_RBAC_USER_ID"));
        this.setCode(getStr(beanMap, "USER_CODE"));
        this.setName(getStr(beanMap, "USER_NAME"));
        this.setPhone(getStr(beanMap, "USER_PHONE"));
        this.setEmail(getStr(beanMap, "USER_MAIL"));
        this.setAvatar(getStr(beanMap, "USER_AVATAR"));
        this.setRemark(getStr(beanMap, "USER_REMARK"));
        this.setPhoto(getStr(beanMap, "USER_PHOTO"));
        this.setDirectLeaderId(getStr(beanMap, "USER_DIRECTOR_ID"));
        this.setDirectLeaderCode(getStr(beanMap, "USER_DIRECTOR_CODE"));
        this.setDirectLeaderName(getStr(beanMap, "USER_DIRECTOR_NAME"));
        this.setSexCode(getStr(beanMap, "USER_SEX_CODE"));
        this.setSexName(getStr(beanMap, "USER_SEX_NAME"));
        this.setMajorId(getStr(beanMap, "USER_MAJOR_ID"));
        this.setMajorCode(getStr(beanMap, "USER_MAJOR_CODE"));
        this.setMajorName(getStr(beanMap, "USER_MAJOR_NAME"));
        this.setMajor("1".equals(getStr(beanMap, "USER_SFZG_CODE")) ? true : false);
        this.setMarried("1".equals(getStr(beanMap, "USER_MARRIED_CODE")) ? true : false);
        this.setCardNum(getStr(beanMap, "USER_CARDNUM"));
        this.setIdTypeCode(getStr(beanMap, "USER_IDTYPE_CODE"));
        this.setIdTypeName(getStr(beanMap, "USER_IDTYPE_NAME"));
        this.setBirthDate(Strings.isNullOrEmpty(getStr(beanMap, "USER_BIRTH")) ? null : DateUtil.parseDate(getStr(beanMap, "USER_BIRTH")));
        this.setAge(getInteger(beanMap, "USER_AGE"));
        this.setEntryDate(Strings.isNullOrEmpty(getStr(beanMap, "USER_ENTRY_DATE")) ? null : DateUtil.parseDate(getStr(beanMap, "USER_ENTRY_DATE")));
        this.setLeaveDate(Strings.isNullOrEmpty(getStr(beanMap, "USER_LEAVE_DATE")) ? null : DateUtil.parseDate(getStr(beanMap, "USER_LEAVE_DATE")));
        this.setPostCode(getStr(beanMap, "USER_POST_CODE"));
        this.setPostName(getStr(beanMap, "USER_POST_NAME"));
        this.setEducationCode(getStr(beanMap, "USER_EDUCATION_CODE"));
        this.setEducationName(getStr(beanMap, "USER_EDUCATION_NAME"));
        this.setNativePlace(getStr(beanMap, "USER_NATIVE_PLACE"));
        this.setInit("1".equals(getStr(beanMap, "USER_INITED_CODE")) ? true : false);
        if (this.getOrganization() instanceof Department) {
            Department department = (Department) this.getOrganization();
            this.setCompanyId(department.getCompanyId());
            this.setCompanyName(department.getCompanyName());
            this.setCompanyCode(department.getCompanyCode());
            this.setGroupCompanyId(department.getGroupCompanyId());
            this.setGroupCompanyName(department.getGroupCompanyName());
            this.setGroupCompanyCode(department.getGroupCompanyCode());
        } else {
            this.setCompanyId(getStr(beanMap, "SY_COMPANY_ID"));
            this.setCompanyName(getStr(beanMap, "SY_COMPANY_NAME"));
            this.setGroupCompanyId(getStr(beanMap, "SY_GROUP_COMPANY_ID"));
            this.setGroupCompanyName(getStr(beanMap, "SY_GROUP_COMPANY_NAME"));
        }
    }

}

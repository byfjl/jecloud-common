# 基础项目

## 项目简介

JECloud基础模块，用于为其他应用服务提供基础能力，包括基础模型，基础服务和基础RPC调用等。

## 环境依赖

* jdk1.8
* maven
* 请使用官方仓库(http://maven.jepaas.com)，暂不同步jar到中央仓库。


> Maven安装 (http://maven.apache.org/download.cgi)

> Maven学习，请参考[maven-基础](docs/mannual/maven-基础.md)

## 服务模块介绍

- jecloud-common-auth： 认证基础适配模块。
- jecloud-common-base: 基础实现。
- jecloud-common-business: 业务服务基础实现。
- jecloud-common-servicecomb: ServiceComb基础框架适配
- jecloud-common-xxljob: xxljob客户端封装
- jecloud-project-dependency: jecloud项目基础依赖封装

## 编译部署

``` shell
//打包
mvn clean package -Dmaven.test.skip=true
//安装本地仓库
mvn clean install -Dmaven.test.skip=true
```

## 开源协议

- [MIT](./LICENSE)
- [平台证书补充协议](./SUPPLEMENTAL_LICENSE.md)

## JECloud主目录
[JECloud 微服务架构低代码平台（点击了解更多）](https://gitee.com/ketr/jecloud.git)